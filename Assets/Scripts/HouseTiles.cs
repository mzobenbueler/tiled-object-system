﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif
public class HouseTile : Tile
{
    [Header("Size")]
    public int minSizeHorizontal;
    public int minSizeVertical;

    [Header("Tiles")]
    public TileBase[] TB_Top;
    public TileBase[] TB_Bottom;
    public TileBase[] TB_Left;
    public TileBase[] TB_Right;
    public TileBase[] TB_Center;
    public TileBase[] TB_TopLeftCorner;
    public TileBase[] TB_TopRightCorner;
    public TileBase[] TB_BottomLeftCorner;
    public TileBase[] TB_BottomRightCorner;

    [Header("Tiled Objects")]
    public TiledObject[] tiledObjects;

    public TileBase[] GetTileArray(Vector2Int size)
    {
        if (size.x < minSizeHorizontal || size.y < minSizeVertical)
        {
            Debug.Log("Demande trop grande");
            return new TileBase[0];
        }

        TileBase[] tileArray = new TileBase[size.x * size.y];

        //Top tiles (depart du dernier etage = size.x * size.y)
        int topStart = size.x * size.y - size.x + 1;
        int zzz = TilePositionToArrayPosition(new Vector2Int(0, size.y), size);
        int topEnd = size.x * size.y - 1;
        int ooo = TilePositionToArrayPosition(new Vector2Int(size.x - 1, size.y), size);

        for (int i = topStart; i < topEnd; i++)
        {
            tileArray[i] = SelectRandomTile(TB_Top);
        }

        //Bottom tiles
        for (int i = 1; i < size.x - 1; i++)
        {
            tileArray[i] = SelectRandomTile(TB_Bottom);
        }

        //Center tiles
        for(int i = 1; i < size.y - 1; i ++)
        {
            for(int j = 1; j < size.x - 1; j ++)
            {
                int z = TilePositionToArrayPosition(new Vector2Int(j, i), size);
                tileArray[z] = SelectRandomTile(TB_Center);
            }
        }

        //Corner tiles 
        int bottomLeft = 0;
        int bottomRight = size.x - 1;
        int topleft = size.x * size.y - size.x;
        int topright = size.x * size.y - 1;
        tileArray[bottomLeft] = SelectRandomTile(TB_BottomLeftCorner);
        tileArray[bottomRight] = SelectRandomTile(TB_BottomRightCorner);
        tileArray[topleft] = SelectRandomTile(TB_TopLeftCorner);
        tileArray[topright] = SelectRandomTile(TB_TopRightCorner);

        //Left and Right side tiles
        int centerStages = size.y - 1;
        for (int i = 1; i < centerStages; i++)
        {
            tileArray[i * size.x] = SelectRandomTile(TB_Left);
            tileArray[i * size.x + size.x - 1] = SelectRandomTile(TB_Right);
        }

        //Add Tiled Object 
        //AddTiledObjects(size, tileArray);

        return tileArray;
    }

    TileBase SelectRandomTile(TileBase[] array)
    {
        TileBase selected = array[Random.Range(0, array.Length)];
        return selected;
    }

    Vector2Int SelectRandomPosition(List<Vector2Int> list)
    {
        Vector2Int selected = list[Random.Range(0, list.Count)];
        return selected;
    }

    //private void AddTiledObjects(Vector2Int fullObjectSize, TileBase[] tileArray)
    //{
    //    foreach(TiledObject to in tiledObjects)
    //    {
    //        //Recevoir les positions possibles ?
    //        //Ou bien recevoir les trucs deja placees
    //        //Il vaudrait mieux que tout le placement se fasse ici 
            
    //        TiledObjectData tod = to.GetTileObjectData(fullObjectSize);
    //        int quantity = UnityEngine.Random.Range(tod.minQuantity, tod.maxQuantity);

    //        for (int i = 0; i < quantity; i ++)
    //        {
    //            if(tod.availablePositions.Count == 0)
    //            {
    //                return;
    //            }

    //            Vector2Int pos = SelectRandomPosition(tod.availablePositions);
    //            //Dessiner toutes les tiles du truc, a cet endroit
    //            for(int j = 0; j < tod.tiles.Length; j ++)
    //            {
    //                int positionIndex = TilePositionToArrayPosition(pos + tod.tilePositions[j], fullObjectSize);
    //                tileArray[positionIndex] = tod.tiles[j];
    //            }
    //        }
    //    }
    //}

    private int TilePositionToArrayPosition(Vector2Int tilePosition, Vector2Int tilemapSize)
    {
        int positionIndex = (tilePosition.y * tilemapSize.x) + tilePosition.x;
        return positionIndex;
    }

#if UNITY_EDITOR
    // The following is a helper that adds a menu item to create a RoadTile Asset
    [MenuItem("Assets/Create/HouseTile")]
    public static void CreateHouseTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Road Tile", "New Road Tile", "Asset", "Save Road Tile", "Assets");
        if (path == "")
            return;
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<HouseTile>(), path);
    }
#endif
}



