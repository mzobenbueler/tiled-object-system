﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;
using System;

[Serializable]
public class TiledObject : ScriptableObject
{
    public BasicTiledObject basicTiledObject = new BasicTiledObject();
    public RP_RulesList rp_rule;

    public void Initialize()
    {
        rp_rule = (RP_RulesList)ScriptableObjectUtility.CreateAndInitialize<RP_RulesList>(this);
    }

#if UNITY_EDITOR
    // Add a menu item to create a Tiled Object
    [MenuItem("Assets/Create/Tiled Object")]
    public static void CreateNewTiledObject()
    {
        string clickedAssetGuid = Selection.assetGUIDs[0];
        string clickedPath = AssetDatabase.GUIDToAssetPath(clickedAssetGuid);
        string path = clickedPath += "/NewTiledObject.Asset";

        if (path == "")
        {
            return;
        }

        TiledObject createAsset = ScriptableObject.CreateInstance<TiledObject>();
        AssetDatabase.CreateAsset(createAsset, path);
        createAsset.Initialize();
    }
#endif
}