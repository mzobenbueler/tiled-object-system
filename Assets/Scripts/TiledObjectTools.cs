﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
using UnityEditor;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Tilemaps;
using UnityEngine.Timeline;

public class TiledObjectTools
{
    public static BasicTiledObject GetGeneratedObject(TiledObject currentTO, params object[] sizeOverride)
    {
        if (!currentTO || currentTO.basicTiledObject.size == Vector2Int.zero)
        {
            return new BasicTiledObject();
        }

        BasicTiledObject generatedBasic = BasicTiledObject.DeepCopy(currentTO.basicTiledObject);
        generatedBasic = ApplyRulesOnBasicObject(generatedBasic, currentTO.rp_rule.GetRulesFromParameter(), sizeOverride);
        return generatedBasic;
    }

    public static BasicTiledObject ApplyRulesOnBasicObject(BasicTiledObject currentObjectResult, List<TiledObjectRule> rulesToApply, params object[] overrideSize)
    {
        foreach(TiledObjectRule rule in rulesToApply)
        {
            var ruleResult = rule.ApplyRuleOnObject(currentObjectResult, overrideSize);
            switch (rule)
            {
                //Modify the size of the resulting object
                case TOR_Modify_Size _:
                    currentObjectResult.size = ruleResult;
                    break;

                //Add Positioned Tiles to the existing object
                case TOR_Add_Tile _:
                case TOR_Add_TiledObject _:
                    currentObjectResult.positionedTiles.AddRange(ruleResult.positionedTiles);
                    break;

                //Replace the existing object by a new one
                case TOR_Add_Grid _:
                case TOR_Add_Grid_new _:
                case TOR_Add_Grid_Third _:
                case TOR_Add_Contour _:
                case TOR_Modify_RepeatTile _:
                case TOR_Modify_RepeatTiledObject _:
                case TOR_Modify_Position p:
                    currentObjectResult = ruleResult;
                    break;
                
                case TiledObjectRule _:
                    Debug.LogWarning("The rule cannot be applied because it is missing in TiledObjectTools switch");
                    break;
            }
        }

        return currentObjectResult;
    }

    public static TiledObjectRule DisplayDropDown(TiledObjectRule existingRule, Editor baseEditor, ScriptableObject mainAsset)
    {
        if (!existingRule)
        {
            existingRule = ScriptableObjectUtility.CreateRuleAsset(Rules.ruleTypes[0], baseEditor, mainAsset) as TiledObjectRule;
            return existingRule;
        }

        int indexSelected = Array.IndexOf(Rules.ruleTypes, existingRule.GetType());

        EditorGUI.BeginChangeCheck();
        indexSelected = EditorGUILayout.Popup(indexSelected, Rules.ruleTypeNames, GUILayout.ExpandWidth(true));
        if (EditorGUI.EndChangeCheck())
        {
            //Remove the old rule
            existingRule.DeleteSelfAndContainedAssets();
            AssetDatabase.RemoveObjectFromAsset(existingRule);

            //Create the new one
            existingRule = ScriptableObjectUtility.CreateRuleAsset(Rules.ruleTypes[indexSelected], baseEditor, mainAsset) as TiledObjectRule;
        }

        return existingRule;
    }
}

[System.Serializable]
public class BasicTiledObject
{
    public List<PositionedTile> positionedTiles;
    public List<TiledObjectRule> objectRules;
    public Vector2Int size;

    public static BasicTiledObject DeepCopy(BasicTiledObject p)
    {
        BasicTiledObject temp = new BasicTiledObject();
        temp.positionedTiles = new List<PositionedTile>();
        foreach(PositionedTile pt in p.positionedTiles)
        {
            if(!pt.tile)
            {
                continue;
            }
            temp.positionedTiles.Add(PositionedTile.DeepCopy(pt));
        }
        temp.objectRules = p.objectRules.ToList();
        temp.size = p.size;

        return temp;
    }
}

[System.Serializable]
public class PositionedTile
{
    public PositionedTile(Tile tile, Vector2Int position)
    {
        this.tile = tile;
        this.position = position;
    }

    public Tile tile;
    public Vector2Int position;

    public static PositionedTile DeepCopy(PositionedTile p)
    {
        PositionedTile temp = new PositionedTile(p.tile, new Vector2Int());
        temp.position.x = p.position.x;
        temp.position.y = p.position.y;
        return temp;
    }
}

public class Rules
{
    public static Type[] ruleTypes;
    public static string[] ruleTypeNames;

    static Rules()
    {
        //Get the available Rules types, to feed the rules drop-down
        Type[] types = System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(TiledObjectRule));

        ruleTypes = types;

        ruleTypeNames = new string[ruleTypes.Length];        
        for(int i = 0; i < ruleTypes.Length; i ++)
        {
            ruleTypeNames[i] = ruleTypes[i].Name;
        }
    }
}

[System.Serializable]
public struct TiledObjectWithRules
{
    public TiledObject tiledObject;
    public List<TiledObjectRule> objectRules;
}