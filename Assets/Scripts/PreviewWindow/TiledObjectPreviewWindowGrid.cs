﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TiledObjectPreviewWindowGrid : Editor
{
    private float currentZoom = 20;
    private Rect previewZoneRect;
    private int fullGridSize = 100;
    private float cellSize;
    private TiledObjectPreviewWindow window;

    public TiledObjectPreviewWindowGrid(TiledObjectPreviewWindow window)
    {
        this.window = window;
        currentZoom = 20;
    }

    private void DrawTiledObjectPreview(List<PositionedTile> tileAndPos)
    {
        for (int i = 0; i < tileAndPos.Count; i++)
        {
            if (!tileAndPos[i].tile)
            {
                Debug.LogWarning(this + " Tile missing in " + window.CurrentObject.name + " at position " + i);
                continue;
            }

            Sprite spr = tileAndPos[i].tile.sprite;
            Vector2Int pos = tileAndPos[i].position;

            DrawSpriteAtPosition(spr, pos);
        }
    }

    private void DrawSpriteAtPosition(Sprite sprite, Vector2Int pos)
    {
        Rect position = new Rect(
            previewZoneRect.x + pos.x * cellSize,
            previewZoneRect.y + previewZoneRect.height - (1 + pos.y) * cellSize,
            cellSize,
            cellSize);

        Vector2 fullSize = new Vector2(sprite.texture.width, sprite.texture.height);
        Vector2 size = new Vector2(sprite.textureRect.width, sprite.textureRect.height);

        Rect coords = sprite.textureRect;
        coords.x /= fullSize.x;
        coords.width /= fullSize.x;
        coords.y /= fullSize.y;
        coords.height /= fullSize.y;

        Vector2 ratio;
        ratio.x = position.width / size.x;
        ratio.y = position.height / size.y;
        float minRatio = Mathf.Min(ratio.x, ratio.y);

        Vector2 center = position.center;
        position.width = size.x * minRatio;
        position.height = size.y * minRatio;
        position.center = center;

        GUI.DrawTextureWithTexCoords(position, sprite.texture, coords);
    }

    public void DrawPreviewGrid(Rect gridRect, List<PositionedTile> tiles)
    {
        previewZoneRect = gridRect;
        previewZoneRect.x = 0;
        previewZoneRect.y = 0;

        GUILayout.Box("", GUIStyle.none);

        //Applying zoom level to cell size
        cellSize = currentZoom * 100 / fullGridSize;

        DrawGridLines();
        DrawTiledObjectPreview(tiles);
    }

    private void DrawGridLines()
    {
        Vector2 previewStartPosition = new Vector2(previewZoneRect.x, previewZoneRect.y + previewZoneRect.height);

        //Horizontal lines
        for (int i = 0; i < fullGridSize + 1; i++)
        {
            Rect r = previewZoneRect;
            r.y = previewStartPosition.y - cellSize * i;
            r.height = 1;
            if (r.y > previewZoneRect.y)
            {
                EditorGUI.DrawRect(r, Color.grey);
            }
        }

        //Vertical lines
        for (int i = 0; i < fullGridSize + 1; i++)
        {
            Rect r = previewZoneRect;
            r.x = r.x + cellSize * i;
            r.x = previewStartPosition.x + cellSize * i;
            r.width = 1;
            EditorGUI.DrawRect(r, Color.grey);
        }
    }

    public void ZoomLevelChange(float changeDirection)
    {
        currentZoom += -changeDirection * 5;
        currentZoom = Mathf.Clamp(currentZoom, 10, 100);
        window.Repaint();
    }
}