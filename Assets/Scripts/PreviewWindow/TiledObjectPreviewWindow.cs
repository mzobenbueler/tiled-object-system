﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.Tilemaps;
using System.Runtime.DesignerServices;
using UnityEngine.UI;

public class TiledObjectPreviewWindow : EditorWindow
{
    //Selected object information
    private TiledObject currentObject;
    public TiledObject CurrentObject
    {
        get { return currentObject; }
    }

    private BasicTiledObject generatedTiledObject;
    private TiledObjectPreviewWindowGrid grid;
    private Rect gridWindowRect = new Rect(0, 0, 0, 0);

    //Add menu named "My Window" to the Window menu
    [MenuItem("Window/2D/Tiled Object Preview")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        TiledObjectPreviewWindow window = (TiledObjectPreviewWindow)EditorWindow.GetWindow(typeof(TiledObjectPreviewWindow), false, "Tiled Object");
        window.Show();

        //window.grid = new TiledObjectPreviewWindowGrid(window);
        window.RefreshGridRect();

        window.CheckActiveObjectToRefreshPreview();
    }

    //Called whenever the selection has changed in the Project window
    public void OnSelectionChange()
    {
        CheckActiveObjectToRefreshPreview();
    }

    private void CheckActiveObjectToRefreshPreview()
    {
        if (Selection.activeObject is TiledObject == false)
        {
            return;
        }

        currentObject = Selection.activeObject as TiledObject;
        RefreshPreview();
    }

    private void RefreshPreview()
    {
        generatedTiledObject = TiledObjectTools.GetGeneratedObject(CurrentObject);

        //if (generatedTiledObject.positionedTiles == null)
        //{
        //    Debug.LogWarning("Tiled Object Preview - On Selection Change : Generated Tiled Object is empty");
        //}

        Repaint();
    }

    private void OnGUI()
    {
        if (!CurrentObject)
        {
            EditorGUILayout.LabelField("Select a Tiled Object");
            return;
        }
        if (generatedTiledObject == null || generatedTiledObject.positionedTiles == null)
        {
            EditorGUILayout.LabelField(CurrentObject.name + " is empty");
            return;
        }
        if (GUILayout.Button("Refresh " + currentObject.name))
        {
            RefreshPreview();
        }

        BeginWindows();
        gridWindowRect = GUILayout.Window(1, gridWindowRect, DoWindow, "Preview");
        EndWindows();

        MouseInput();
    }

    private void DoWindow(int unusedWindowID)
    {
        //This should occur only when the window is resized
        //Implement OnResize();
        RefreshGridRect();

        grid.DrawPreviewGrid(gridWindowRect, generatedTiledObject.positionedTiles);
    }

    private void RefreshGridRect()
    {
        if(!grid)
        {
            grid = new TiledObjectPreviewWindowGrid(this);
        }

        int horizontalOffset = 6;

        gridWindowRect.x = horizontalOffset;
        gridWindowRect.y = EditorGUIUtility.singleLineHeight * 2;
        gridWindowRect.width = Screen.width - horizontalOffset * 2;
        gridWindowRect.height = Screen.height - EditorGUIUtility.singleLineHeight * 5;
    }        

    private void MouseInput()
    {
        Event m_Event = Event.current;

        if (m_Event.type == EventType.ScrollWheel)
        {
            grid.ZoomLevelChange(Mathf.Sign(m_Event.delta.y));
        }
    }
}