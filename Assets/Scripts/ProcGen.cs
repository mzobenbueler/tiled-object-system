﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
public struct Line
{
    public int Size;
    public bool bHorizontal;
    public bool bDirectionPositive;
    public Vector2Int StartPosition;
}

public class ProcGen : MonoBehaviour
{
    public Tilemap tilemap;
    public TileBase selectedTile; 

    public int gridWidth;
    public int gridHeight;

    public int linesQuantity;
    public int lineSizeMin;
    public int lineSizeMax;

    public int[,] grid;

    public Button generateButton;

    public HouseTile houseTile;

    // Start is called before the first frame update
    void Start()
    {
        //tilemap.SetTile(new Vector3Int(0, 0, 0), selectedTile); Test pour setter un tile
        generateButton.onClick.AddListener(GenerateNewTilemap);

        GridInitialization();
        CreateLines(linesQuantity);
        LogGrid();
    }

    void GridInitialization()
    {
        //Grid initialization
        grid = new int[gridWidth, gridHeight];

        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                grid[i, j] = 0;
            }
        }
    }

    void CreateLines(int linesQuantity)
    {
        //Create Line objects
        Line[] Lines = new Line[linesQuantity];
        for(int i = 0; i < Lines.Length; i ++)
        {
            Lines[i] = new Line();
            Lines[i].Size = Random.Range(lineSizeMin, lineSizeMax);
            Lines[i].bHorizontal = Random.Range(0, 2) == 1;
            Lines[i].bDirectionPositive = Random.Range(0, 2) == 1;
            Lines[i].StartPosition.x = Random.Range(0, gridWidth);
            Lines[i].StartPosition.y = Random.Range(0, gridHeight);

            //string debug = "Line created :";
            //debug += "Position = " + Lines[i].StartPosition.x + ", " + Lines[i].StartPosition.y;
            //debug += " Size = " + Lines[i].Size;
            //debug += " Horizontal = " + Lines[i].bHorizontal + " Positive = " + Lines[i].bDirectionPositive;
            //Debug.Log(debug);
        }

        //Color line
        int tempIndex = 0;
        foreach (Line line in Lines)
        {
            tempIndex += 1;
            Vector2Int[] positionsToChange = new Vector2Int[line.Size];
            for (int i = 0; i < positionsToChange.Length; i++)
            {
                positionsToChange[i] = line.StartPosition;
                Vector2Int nextPos = Vector2Int.zero;

                //message = health > 0 ? "Player is Alive" : "Player is Dead";
                nextPos = line.bHorizontal ? Vector2Int.right : Vector2Int.up;
                nextPos = line.bDirectionPositive ? nextPos : -nextPos;

                nextPos *= i;

                positionsToChange[i] += nextPos;

                positionsToChange[i].x = Mathf.Clamp(positionsToChange[i].x, 0, gridWidth - 1);
                positionsToChange[i].y = Mathf.Clamp(positionsToChange[i].y, 0, gridHeight - 1);

                grid[positionsToChange[i].x, positionsToChange[i].y] = tempIndex;

                tilemap.SetTile(new Vector3Int(positionsToChange[i].x, positionsToChange[i].y, 0), selectedTile); //Test pour setter un tile

            }
        }        
    }

    void GenerateNewTilemap()
    {
        GridInitialization();

        tilemap.ClearAllTiles();
        CreateLines(linesQuantity);

        LogGrid();

        Vector2Int houseSize = new Vector2Int(Random.Range(3, 10), Random.Range(3,10));
        AddHouse(Vector2Int.zero, houseSize);
    }

    void LogGrid()
    {
        for(int j = 0; j < gridHeight; j++)
        {
            string debugLine = "";

            for (int i = 0; i < gridWidth; i++)
            {
                debugLine += grid[i, j];
                debugLine += ". ";
            }
            //Debug.Log(debugLine);
        }
    }

    void AddHouse(Vector2Int worldPosition, Vector2Int size)
    {
        //Recuperer un array de Tilebases depuis le tile
        TileBase[] tiles = houseTile.GetTileArray(size);

        Vector3Int[] positions = new Vector3Int[size.x * size.y];
        for (int i = 0; i < positions.Length; i++)
        {
            positions[i] = new Vector3Int(i % size.x + worldPosition.x, i / size.y + worldPosition.y, 0);
        }

        int index = 0;
        for (int i = 0; i < size.y; i++)
        {
            for(int j = 0; j < size.x; j++)
            {
                positions[index] = new Vector3Int(worldPosition.x + j, worldPosition.y + i, 0);
                index ++;
            }
        }
        tilemap.SetTiles(positions, tiles);
    }
}