﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public static class ScriptableObjectUtility
{
    public static ScriptableObject CreateAndInitialize<T>(ScriptableObject assetToSaveIn) where T : RuleParameter
    {
        T createAsset = ScriptableObject.CreateInstance<T>();
        return InitializeAndSaveParameter(createAsset, assetToSaveIn);
    }

    public static ScriptableObject CreateAndInitialize(Type objectType, ScriptableObject assetToSaveIn)
    {
        RuleParameter createAsset = ScriptableObject.CreateInstance(objectType) as RuleParameter;
        return InitializeAndSaveParameter(createAsset, assetToSaveIn);
    }

    public static ScriptableObject CreateRuleAsset(Type ruleType, Editor baseEditor, ScriptableObject assetToSaveIn)
    {
        TiledObjectRule createAsset = ScriptableObject.CreateInstance(ruleType) as TiledObjectRule;
        return InitializeAndSaveRule(createAsset, assetToSaveIn);
    }

    public static ScriptableObject InitializeAndSaveParameter(RuleParameter createAsset, ScriptableObject assetToSaveIn)
    {
        AssetDatabase.AddObjectToAsset(createAsset, assetToSaveIn);
        createAsset.mainAsset = assetToSaveIn;
        createAsset.name = createAsset.GetType().ToString();
        createAsset.InitializeParameters(assetToSaveIn);
        AssetDatabase.SaveAssets();

        return createAsset;
    }

    public static ScriptableObject InitializeAndSaveRule(TiledObjectRule createAsset, ScriptableObject assetToSaveIn)
    {
        AssetDatabase.AddObjectToAsset(createAsset, assetToSaveIn);
        createAsset.name = createAsset.GetType().ToString();
        createAsset.Initialize();
        AssetDatabase.SaveAssets();

        return createAsset;
    } 
}