﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.IO;
using System;
using UnityEngine.Tilemaps;
using UnityEditor.AnimatedValues;
using UnityEngine.Events;

[CustomEditor(typeof(TiledObject))]
[CanEditMultipleObjects]
public class TiledObjectEditor : Editor
{
    SerializedProperty basicTiledObjectSize;
    RP_RulesList rp_rules;

    void OnEnable()
    {
        SerializedProperty basicTiledObject = serializedObject.FindProperty("basicTiledObject");

        basicTiledObjectSize = basicTiledObject.FindPropertyRelative("size");

        SerializedProperty rules = serializedObject.FindProperty("rp_rule");
        rp_rules = (RP_RulesList)rules.objectReferenceValue;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(basicTiledObjectSize, new GUIContent("Size"));

        DrawUILine(Color.black);

        rp_rules.DisplayParameter();

        DrawUILine(Color.black);

        serializedObject.ApplyModifiedProperties();
    }

    //Move this in a EditorGUI Tools class
    public static void DrawUILine(Color color, int thickness = 2, int padding = 10, int leftPadding = 0)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2 - leftPadding * 10;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }
}