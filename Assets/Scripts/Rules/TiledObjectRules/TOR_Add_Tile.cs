﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TOR_Add_Tile : TiledObjectRule
{
    [SerializeField] private RP_RulesList rp_rule;
    [SerializeField] private RP_Tile rp_tile;

    public override void Initialize()
    {
        rp_rule = (RP_RulesList)ScriptableObjectUtility.CreateAndInitialize<RP_RulesList>(this);
        rp_tile = (RP_Tile)ScriptableObjectUtility.CreateAndInitialize<RP_Tile>(this);
    }

    public override void DeleteSelfAndContainedAssets()
    {
        rp_rule.RemoveNestedParametersAndSelf();
        rp_tile.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        BasicTiledObject obj = rp_tile.GenerateBasicTiledObjectFromParameter(options);    
        List<TiledObjectRule> rules = rp_rule.GetRulesFromParameter();

        Vector2Int objSize = currentObject.size;
        if (options.Length > 0)
        {
            objSize = (Vector2Int)options[0];
        }

        obj = TiledObjectTools.ApplyRulesOnBasicObject(obj, rules, objSize);

        return obj; 
    }

    public override void DisplayParameters()
    {
        rp_tile.DisplayParameter();
        rp_rule.DisplayParameter();
    }
}