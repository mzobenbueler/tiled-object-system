﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TOR_Add_Grid : TiledObjectRule
{
    //Grid
    //Values from RuleParameters objects
    [SerializeField] private RP_DropDownInt RP_GridSize_TilesX;
    [SerializeField] private RP_DropDownInt RP_GridSize_TilesY;

    [SerializeField] private RP_DropDownInt RP_GridSize_CellsX;
    [SerializeField] private RP_DropDownInt RP_GridSize_CellsY;

    [SerializeField] private RP_GridRowsSizes RP_GridRows_Sizes;

    //Fixed values from RuleParameters, saved in this object
    //Grid size by Tiles
    [SerializeField] private Vector2Int gridSize_Tiles_Minimal = Vector2Int.one;
    [SerializeField] private Vector2Int gridSize_Tiles_Maximal = Vector2Int.one;

    //Grid size by Cells
    [SerializeField] private Vector2Int gridSize_Cells_Minimal = Vector2Int.one;
    [SerializeField] private Vector2Int gridSize_Cells_Maximal = Vector2Int.one;

    //Based on RP_GridRows_Sizes
    [SerializeField] private RowSizes rowSizes_Tiles_Minimal;
    [SerializeField] private RowSizes rowSizes_Tiles_Maximal;

    //Values calculated from parameters
    [SerializeField] private Vector2Int gridSize_Tiles_BasedOnCells_Minimal = Vector2Int.zero;
    [SerializeField] private Vector2Int gridSize_Tiles_BasedOnCells_Maximal = Vector2Int.zero;

    //UI
    bool showGridSizeValues = false;
    bool showGridSizeTilesValues = false;
    bool showGridSizeCellsValues = false;
    bool showGridRowSizesValues = false;
    bool showGridObjects = false;

    //TiledObjects disponibles a chaque positions
    [SerializeField] private PositionsListsAncien availableObjectsByPositions;
    [SerializeField] private List<GridCellWrapper> gridCells = new List<GridCellWrapper>();
    [SerializeField] private List<GridTiledObjectAncien> tiledObjectsWithPosition = new List<GridTiledObjectAncien>();

    public override void Initialize()
    {
        //Grid params initialization
        gridCells.Add(new GridCellWrapper());
        gridCells[0].innerList.Add(new GridCell());

        //Rule Parameters initialization
        RP_GridSize_TilesX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_TilesY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_CellsX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_CellsY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridRows_Sizes = (RP_GridRowsSizes)ScriptableObjectUtility.CreateAndInitialize<RP_GridRowsSizes>(this);

        rowSizes_Tiles_Minimal = new RowSizes();
        rowSizes_Tiles_Maximal = new RowSizes();

        availableObjectsByPositions = new PositionsListsAncien();

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        RP_GridSize_TilesX.RemoveNestedParametersAndSelf();
        RP_GridSize_TilesY.RemoveNestedParametersAndSelf();
        RP_GridSize_CellsX.RemoveNestedParametersAndSelf();
        RP_GridSize_CellsY.RemoveNestedParametersAndSelf();
        RP_GridRows_Sizes.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        //Use options param size or grid parameter size
        Vector2Int desiredSize = options.Length > 0 ? (Vector2Int)options[0] : new Vector2Int(RP_GridSize_TilesX.GetIntFromParameter(currentObject, options), RP_GridSize_TilesY.GetIntFromParameter(currentObject, options));

        string selectedSize = options.Length > 0 ? "from option param" : "from RP_GridSize_Tile";

        Debug.Log("GENERATING NEW GRID. Desired size = " + desiredSize + " " + selectedSize);

        //Verify if the requested size is correct
        if (!CheckIfAnObjectOfThisSizeCanBeCreated(desiredSize))
        {
            return new BasicTiledObject();
        }

        GeneratedGrid gg = new GeneratedGrid();
        gg.heightsAndWidths = GenerateRowsBasedOnDesiredSize(desiredSize);
        
        //Prendre des objets au hasard et les placer dans chaque case
        Vector2Int gridSize_Cells = new Vector2Int(gg.heightsAndWidths.widths.Length, gg.heightsAndWidths.heights.Length);
        Vector2Int currentCell_TilePosition = Vector2Int.zero;
        Vector2Int currentCell_CellPosition = Vector2Int.zero;
        BasicTiledObject finalObject = null;
        object[] sizeOverride = new object[1] { Vector2Int.zero };

        for (int i = 0; i < gridSize_Cells.x; i ++)
        {
            if (i > 0)
            {
                currentCell_TilePosition.x += gg.heightsAndWidths.widths[i - 1];
            }
            currentCell_TilePosition.y = 0;
            currentCell_CellPosition.x = i;

            for(int j = 0; j < gridSize_Cells.y; j ++)
            {
                if(j > 0)
                {
                    currentCell_TilePosition.y += gg.heightsAndWidths.heights[j - 1];
                }
                currentCell_CellPosition.y = j;

                Debug.Log("Next position to generate : " + currentCell_CellPosition);

                TiledObject tempTO = availableObjectsByPositions.GetTiledObjectForThisPosition(currentCell_CellPosition, gridSize_Cells);
                if(!tempTO)
                {
                    Debug.LogWarning(this + " These is no Tiled Object for the position " + currentCell_CellPosition);
                    continue;
                }

                sizeOverride[0] = new Vector2Int(gg.heightsAndWidths.widths[i], gg.heightsAndWidths.heights[j]);
                BasicTiledObject tempResult = TiledObjectTools.GetGeneratedObject(tempTO, sizeOverride);

                //Offset tiles position if necessary
                if (currentCell_TilePosition != Vector2Int.zero)
                {
                    for (int h = 0; h < tempResult.positionedTiles.Count; h++)
                    {
                        tempResult.positionedTiles[h].position += currentCell_TilePosition;
                    }
                }

                //Create the first object or add the new ones
                if(finalObject == null)
                {
                    finalObject = tempResult;
                }
                else
                {
                    finalObject.positionedTiles.AddRange(tempResult.positionedTiles);
                }
            }
        }
        return finalObject;
    }

    #region Grid Rows Generation
    private RowSizes GenerateRowsBasedOnDesiredSize(Vector2Int requestedGridSize_Tiles)
    {
        RowSizes generatedRowSizes = new RowSizes();

        //Creer les row horizontales
        Debug.Log("Generating Horizontal Rows");
        CellsAndTilesQuantities horizontalValues = GetMaximumCellsQuantityAndRestingTiles(requestedGridSize_Tiles.x, gridSize_Cells_Minimal.x, gridSize_Cells_Maximal.x, rowSizes_Tiles_Minimal.widths, rowSizes_Tiles_Maximal.widths);        
        int[] horizontalRows = CreateRowSizesList(horizontalValues.cells, rowSizes_Tiles_Minimal.widths);        
        generatedRowSizes.widths = ExpendCellsToFitInGrid(horizontalRows, horizontalValues.tiles, rowSizes_Tiles_Minimal.widths, rowSizes_Tiles_Maximal.widths);

        //Creer les row verticales
        Debug.Log("Generating Vertical Rows");
        CellsAndTilesQuantities verticalValues = GetMaximumCellsQuantityAndRestingTiles(requestedGridSize_Tiles.y, gridSize_Cells_Minimal.y, gridSize_Cells_Maximal.y, rowSizes_Tiles_Minimal.heights, rowSizes_Tiles_Maximal.heights);
        int[] verticalRowSizes = CreateRowSizesList(verticalValues.cells, rowSizes_Tiles_Minimal.heights);
        generatedRowSizes.heights = ExpendCellsToFitInGrid(verticalRowSizes, verticalValues.tiles, rowSizes_Tiles_Minimal.heights, rowSizes_Tiles_Maximal.heights);

        return generatedRowSizes;
    }

    private CellsAndTilesQuantities GetMaximumCellsQuantityAndRestingTiles(int desiredTilesQuantity, int cellsMinimumQuantity, int cellsMaximumQuantity, int[] currentRow_SizesMin, int[] currentRow_SizesMax)
    {
        //1. Du min au max, ajouter des cells
        //A partir de cells min, 
        //2. s'arreter quadn on depasse. Voir combien ça fait de cellules et combien y'a de cases libres

        //Pour une ligne : 
        //Demarrer de la cellule minimum
        int cellIndex = cellsMinimumQuantity;
        //Demarrer du bon nombre de tile, en fonction de la cellule minimum

        int totalCellsSize = GetRowLenghtBasedOnRowSizes(cellIndex, currentRow_SizesMin);

        while (cellIndex < cellsMaximumQuantity)        
        {
            int sizeIfNewCellIsAdded = totalCellsSize + GetSingleRowSizeBasedOnItsPosition(cellIndex, currentRow_SizesMin);
            if (sizeIfNewCellIsAdded <= desiredTilesQuantity)
            {
                cellIndex++;
                totalCellsSize += GetSingleRowSizeBasedOnItsPosition(cellIndex, currentRow_SizesMin);
            } 
            else
            {
                break;
            }
        };

        CellsAndTilesQuantities result = new CellsAndTilesQuantities();
        result.cells = cellIndex;
        result.tiles = desiredTilesQuantity - totalCellsSize;

        Debug.Log("Need : " + result.cells + " cell(s) (" + totalCellsSize + " tiles), plus " + result.tiles + " tile(s), to reach minimal size");

        if (!CheckIfTheObjectCanBeCreatedWithTheseCells(result.cells, desiredTilesQuantity, currentRow_SizesMax))
        {
            Debug.Log("IMPOSSIBLE");
            return null;
        }
        else
        {
            return result;
        }
    }

    private int[] CreateRowSizesList(int cellQuantity, int[] minimalRowSizes)
    {
        int[] rowSizes = new int[cellQuantity];
        for(int i = 0; i < cellQuantity; i ++)
        {
            rowSizes[i] = GetSingleRowSizeBasedOnItsPosition(i, minimalRowSizes);
        }
        return rowSizes;
    }

    private bool CheckIfTheObjectCanBeCreatedWithTheseCells(int cellsQuantity, int tileQuantityToReach, int[] currentRow_SizesMax)
    {
        Debug.Log("Is it possible to complete the gird ?");

        /* Check if it is possible to complete the grid with the maximum sizes. */
        int currentTileQuantity = 0;
        int currentCellIndex = 0;

        while (currentCellIndex < cellsQuantity)
        {
            //Ajouter la taille de la cell actuelle a currenttilequantiyty
            currentTileQuantity += GetSingleRowSizeBasedOnItsPosition(currentCellIndex, currentRow_SizesMax);
            currentCellIndex++;
        };

        if (currentTileQuantity >= tileQuantityToReach)
        {
            return true;
        }
        else
        {
            Debug.LogWarning("Cannot reach desired size (" + tileQuantityToReach + ")");
            return false;
        }
    }

    private int[] ExpendCellsToFitInGrid(int[] rowSizes, int tileQuantityToAdd, int[] rowSizesTilesMin, int [] rowSizesTilesMax)
    {
        //Faire une liste des cells agrandissables 
        List<int> extendableCellsIndexes = new List<int>();
        for(int i = 0; i < rowSizes.Length; i ++)
        {
            //Compare minsize and maxsize for this cell
            int currentCellMin = GetSingleRowSizeBasedOnItsPosition(i, rowSizesTilesMin);
            int currentCellMax = GetSingleRowSizeBasedOnItsPosition(i, rowSizesTilesMax);

            //Add it the list of enlargeable cells
            if(currentCellMin != currentCellMax)
            {
                Debug.Log("Line " + i + " can be exprended : " + currentCellMin + " to " + currentCellMax);
                extendableCellsIndexes.Add(i);
            }                
        }

        //Pour chaque tile manquante, choisir un item au hasard dans cette liste et lui ajouter 1 tile
        for (int i = 0; i < tileQuantityToAdd; i ++)
        {
            int randomItemIndex = UnityEngine.Random.Range(0, extendableCellsIndexes.Count);
            int rowToExpendIndex = extendableCellsIndexes[randomItemIndex];

            rowSizes[rowToExpendIndex] += 1;

            //Enlever l'item de la liste si il n'a plus de places
            if(rowSizes[rowToExpendIndex] == GetSingleRowSizeBasedOnItsPosition(rowSizes[rowToExpendIndex], rowSizesTilesMax))
            {
                extendableCellsIndexes.RemoveAt(randomItemIndex);
            }
        }

        return rowSizes;
    }
    
    private int GetSingleRowSizeBasedOnItsPosition(int rowPosition, int[] rowSizes)
    {
        if (rowPosition == 0)
        {
            return rowSizes[0];
        }
        else if (rowPosition == 1)
        {
            return rowSizes[2];
        }
        else
        {
            return rowSizes[1];
        }
    }

    private bool CheckIfAnObjectOfThisSizeCanBeCreated(Vector2Int desiredSize)
    {
        if (desiredSize.x < gridSize_Tiles_BasedOnCells_Minimal.x || desiredSize.y < gridSize_Tiles_BasedOnCells_Minimal.y)
        {
            Debug.LogWarning(this + " Desired Size (" + desiredSize + ") is shorter than minimal gridSize (" + gridSize_Tiles_BasedOnCells_Minimal + ")");
            return false;
        }
        else if (desiredSize.x > gridSize_Tiles_BasedOnCells_Maximal.x || desiredSize.y > gridSize_Tiles_BasedOnCells_Maximal.y)
        {
            Debug.LogWarning(this + " Desired Size (" + desiredSize + ") is greater than maximal gridSize (" + gridSize_Tiles_BasedOnCells_Maximal + ")");
            return false;
        }

        return true;
    }

    #endregion

    #region Editor UI
    public override void DisplayParameters()
    {
        showGridSizeValues = EditorGUILayout.Foldout(showGridSizeValues, "Grid Size");
        if(showGridSizeValues)
        {
            EditorGUI.indentLevel++;

            EditorGUI.BeginChangeCheck();

            DisplayGridSizeTilesParameters();
            DisplayGridSizeCellsParameters();
            DisplayRowSizeParameters();
            
            if (EditorGUI.EndChangeCheck())
            {
                RefreshGridSizeTilesBasedOnRowSizes();
            }

            EditorGUILayout.Space();

            DisplayGridSizeCalculatedValues();

            EditorGUILayout.Space();

            EditorGUI.indentLevel--;
        }

        showGridObjects = EditorGUILayout.Foldout(showGridObjects, "Tiled Objects");
        if(showGridObjects)
        {
            EditorGUI.BeginChangeCheck();

            DisplayAvailableTiledObjectsList();

            if(EditorGUI.EndChangeCheck())
            {
                //Ancien
                //RefreshObjectPositionsLists();

                //New
                availableObjectsByPositions = PositionsListsAncien.GenerateObjectPositionsLists(tiledObjectsWithPosition);
            }
        }

        EditorUtility.SetDirty(this);
    }

    private void DisplayGridSizeTilesParameters()
    {
        string titleSectionTiles = "Size (Tiles) : ";            
        titleSectionTiles += gridSize_Tiles_Minimal == gridSize_Tiles_Maximal ?
            gridSize_Tiles_Minimal.ToString() :
            gridSize_Tiles_Minimal + " to " + gridSize_Tiles_Maximal;

        showGridSizeTilesValues = EditorGUILayout.Foldout(showGridSizeTilesValues, titleSectionTiles);
        if(showGridSizeTilesValues)
        {
            EditorGUILayout.LabelField("Horizontal Size");
            RP_GridSize_TilesX.DisplayParameter();
            EditorGUILayout.LabelField("Vertical Size");
            RP_GridSize_TilesY.DisplayParameter();
        }
    }

    private void DisplayGridSizeCellsParameters()
    {
        string titleSectionCells = "Size (Cells) : ";
        titleSectionCells += gridSize_Cells_Minimal == gridSize_Cells_Maximal ?
            gridSize_Cells_Minimal.ToString() :
            gridSize_Cells_Minimal + " to " + gridSize_Cells_Maximal;

        showGridSizeCellsValues = EditorGUILayout.Foldout(showGridSizeCellsValues, titleSectionCells);
        if (showGridSizeCellsValues)
        {
            EditorGUILayout.LabelField("Horizontal Size");
            RP_GridSize_CellsX.DisplayParameter();
            EditorGUILayout.LabelField("Vertical Size");
            RP_GridSize_CellsY.DisplayParameter();
        }
    }

    private void DisplayRowSizeParameters()
    {
        string titleSectionRows = "Rows (Tiles) :";

        string widthsTitle = " Widths : ";
        for (int i = 0; i < rowSizes_Tiles_Minimal.widths.Length; i ++)
        {
            widthsTitle += rowSizes_Tiles_Minimal.widths[i] == rowSizes_Tiles_Maximal.widths[i] ? 
                "| " + rowSizes_Tiles_Minimal.widths[i].ToString() + " |":
                "| " + rowSizes_Tiles_Minimal.widths[i] + " - " + rowSizes_Tiles_Maximal.widths[i] + " |";
        }

        string heightsTitle = " Heights : ";
        for (int i = 0; i < rowSizes_Tiles_Minimal.heights.Length; i++)
        {
            heightsTitle += rowSizes_Tiles_Minimal.heights[i] == rowSizes_Tiles_Maximal.heights[i] ?
                "| " + rowSizes_Tiles_Minimal.heights[i].ToString() + " |" :
                "| " + rowSizes_Tiles_Minimal.heights[i] + " - " + rowSizes_Tiles_Maximal.heights[i] + " |";
        }

        titleSectionRows += widthsTitle + "," + heightsTitle;

        showGridRowSizesValues = EditorGUILayout.Foldout(showGridRowSizesValues, titleSectionRows, true);
        if (showGridRowSizesValues)
        {
            RP_GridRows_Sizes.DisplayParameter();
        }
    }

    private void DisplayGridSizeCalculatedValues()
    {
        string calculatedSize = gridSize_Tiles_BasedOnCells_Minimal == gridSize_Tiles_BasedOnCells_Maximal ?
            "Calculated grid size (Tiles): " + gridSize_Tiles_BasedOnCells_Minimal:
            "Calculated grid size (Tiles): " + gridSize_Tiles_BasedOnCells_Minimal + " to " + gridSize_Tiles_BasedOnCells_Maximal;

        EditorGUILayout.LabelField(calculatedSize);

        if (gridSize_Tiles_Minimal.x < gridSize_Tiles_BasedOnCells_Minimal.x || gridSize_Tiles_Minimal.y < gridSize_Tiles_BasedOnCells_Minimal.y)
        {
            EditorGUILayout.LabelField("CANNOT GENERATE GRID");

            EditorGUILayout.LabelField("Minimal grid size (Tiles : " + gridSize_Tiles_Minimal + ") is shorter than calculated minimal grid size (" + gridSize_Tiles_BasedOnCells_Minimal + ")");
        }
        if (gridSize_Tiles_Maximal.x > gridSize_Tiles_BasedOnCells_Maximal.x || gridSize_Tiles_Maximal.y > gridSize_Tiles_BasedOnCells_Maximal.y)
        {
            EditorGUILayout.LabelField("CANNOT GENERATE GRID");

            EditorGUILayout.LabelField("Maximal grid size (Tiles) (" + gridSize_Tiles_Maximal + ")  is greater than calculated maximal grid size (Tiles : " + gridSize_Tiles_BasedOnCells_Maximal + ")");
        }
    }

    private void DisplayAvailableTiledObjectsList()
    {
        /* Display the list of available tiled objects for the grid */
        if(tiledObjectsWithPosition.Count == 0)
        {
            if (GUILayout.Button("Add Item"))
            {
                tiledObjectsWithPosition.Add(new GridTiledObjectAncien());
            }
            return;
        }

        for (int i = 0; i < tiledObjectsWithPosition.Count; i ++)
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Item"))
            {
                tiledObjectsWithPosition.Add(new GridTiledObjectAncien());
            }
            if (GUILayout.Button("Delete Item"))
            {
                tiledObjectsWithPosition.RemoveAt(i);
                EditorGUILayout.EndHorizontal();
                return;
            }

            EditorGUILayout.EndHorizontal();

            tiledObjectsWithPosition[i].tiledObject = (TiledObject)EditorGUILayout.ObjectField("Tiled Object", tiledObjectsWithPosition[i].tiledObject, typeof(TiledObject), false);

            /* Checkboxes corresponding to the available position(s) for this object*/
            List<bool> pos = tiledObjectsWithPosition[i].availablePositions;
            int positionInArrayIndex = 0;
            for(int j = 0; j < 3; j ++)
            {
                EditorGUILayout.BeginHorizontal();

                for(int h = 0; h < 3; h ++)
                {
                    pos[positionInArrayIndex] = EditorGUILayout.Toggle(pos[positionInArrayIndex]);
                    positionInArrayIndex++;
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
    #endregion

    #region Editor UI functions
    private void RefreshGridSizeTilesBasedOnRowSizes()
    {
        //A SUPPRIMER quand les parametres ne seront plus necessaires
        BasicTiledObject currentObject = new BasicTiledObject();
        object[] options = new object[0];

        //Get grid size in Tiles, based on parameters
        gridSize_Tiles_Minimal = new Vector2Int(
            RP_GridSize_TilesX.GetMinimumIntFromParameter(currentObject, options), 
            RP_GridSize_TilesY.GetMinimumIntFromParameter(currentObject, options));
        gridSize_Tiles_Maximal = new Vector2Int(
            RP_GridSize_TilesX.GetMaximumIntFromParameter(currentObject, options),
            RP_GridSize_TilesY.GetMaximumIntFromParameter(currentObject, options));

        //Get grid size in Cells, based on parameters
        gridSize_Cells_Minimal = new Vector2Int(
            RP_GridSize_CellsX.GetMinimumIntFromParameter(currentObject, options),
            RP_GridSize_CellsY.GetMinimumIntFromParameter(currentObject, options));
        gridSize_Cells_Maximal = new Vector2Int(
            RP_GridSize_CellsX.GetMaximumIntFromParameter(currentObject, options),
            RP_GridSize_CellsY.GetMaximumIntFromParameter(currentObject, options));

        //Get rows sizes based, based on parameters
        rowSizes_Tiles_Minimal.widths = RP_GridRows_Sizes.GetMinimumWidths(currentObject, options);
        rowSizes_Tiles_Minimal.heights = RP_GridRows_Sizes.GetMinimumHeights(currentObject, options);
        rowSizes_Tiles_Maximal.widths = RP_GridRows_Sizes.GetMaximumWidths(currentObject, options);
        rowSizes_Tiles_Maximal.heights = RP_GridRows_Sizes.GetMaximumHeights(currentObject, options);

        //Get grid size in Tiles, based on Cells quantity and sizes
        gridSize_Tiles_BasedOnCells_Minimal = new Vector2Int(
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Minimal.x, rowSizes_Tiles_Minimal.widths),
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Minimal.y, rowSizes_Tiles_Minimal.heights));
        gridSize_Tiles_BasedOnCells_Maximal = new Vector2Int(
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Maximal.x, rowSizes_Tiles_Maximal.widths),
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Maximal.y, rowSizes_Tiles_Maximal.heights));
    }
    #endregion

    private int GetRowLenghtBasedOnRowSizes(int cellQuantity, int[] columnsOrLinesSizes)
    {
        /* Returns the length (in Tiles) of a row depending on the number of Cells and the size of each row */

        int resultSize = 0;

        for(int i = 0; i < cellQuantity; i ++)
        {
            if(i == 0)
            {
                resultSize += columnsOrLinesSizes[0];
            }
            else if(i == cellQuantity - 1)
            {
                resultSize += columnsOrLinesSizes[2];
            }
            else
            {
                resultSize += columnsOrLinesSizes[1];
            }
        }

        return resultSize;
    }
}
