﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class TiledObjectRule : ScriptableObject
{

    public virtual dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        Debug.LogWarning(this + " ApplyRuleOnObject function is missing from script");
        return null;
    }

    public virtual void Initialize()
    {
        /* Called just after the creation of the Tiled Object Rule */
    }

    public virtual void DeleteSelfAndContainedAssets()
    {
        Debug.LogWarning("Function missing in " + this + ". Delete this message if it is displayed for no reason");
    }

    public virtual void DisplayParameters()
    {
        UnityEditor.EditorGUILayout.LabelField("No parameter to display in " + this);
    }

    [System.Serializable]
    public enum FillZone_Quantity
    {
        FixedQuantity = 0,
        Random = 1,
        Fill = 2
    }
        
    [System.Serializable]
    public enum FillZone_RepeatMode
    {
        Disabled = 0,
        Manual = 1
    }

    [System.Serializable]
    public enum RANDOMPOSITIONMODE
    {
        BetweenMinAndMax = 0,
        ParentObjectValue = 1
    }

    [System.Serializable]
    public enum POSITION
    {
        TopLeft = 0,
        Top = 1,
        TopRight = 2,
        Left = 3,
        Center = 4,
        Right = 5,
        BottomLeft = 6,
        Bottom = 7,
        BottomRight = 8
    }

    [System.Serializable]
    public enum SIZEOPTIONS
    {
        Manual = 0,
        BasedOnParentObject = 1,
        Random = 2,
        NoChange = 3
    }

    [System.Serializable]
    public enum PARENTSIZEOPTIONS
    {
        ParentObjectWidth = 0,
        ParentObjectHeight = 1,
    }

    [System.Serializable]
    public enum INCLUDEBORDERS
    {
        IncludeBorders = 0,
        Minus1Border = 1,
        Minus2Borders = 2,
    }


}