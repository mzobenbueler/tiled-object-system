﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TOR_Add_Grid_new : TiledObjectRule
{
    //Grid
    //Values from RuleParameters objects
    //[SerializeField] private RP_DropDownInt RP_GridSize_TilesX;
    //[SerializeField] private RP_DropDownInt RP_GridSize_TilesY;

    [SerializeField] private RP_DropDownInt RP_GridSize_CellsX;
    [SerializeField] private RP_DropDownInt RP_GridSize_CellsY;
    [SerializeField] private RP_GridRowsSizes RP_GridRows_Sizes;

    //Fixed values from RuleParameters, saved in this object
    //Grid size by Tiles
    [SerializeField] private Vector2Int gridSize_Tiles_Minimal = Vector2Int.one;
    [SerializeField] private Vector2Int gridSize_Tiles_Maximal = Vector2Int.one;

    //Grid size by Cells
    [SerializeField] private Vector2Int gridSize_Cells_Minimal = Vector2Int.one;
    [SerializeField] private Vector2Int gridSize_Cells_Maximal = Vector2Int.one;

    //Based on RP_GridRows_Sizes
    [SerializeField] private RowSizes rowSizes_Tiles_Minimal;
    [SerializeField] private RowSizes rowSizes_Tiles_Maximal;

    //Values calculated from parameters
    [SerializeField] private Vector2Int gridSize_Tiles_BasedOnCells_Minimal = Vector2Int.zero;
    [SerializeField] private Vector2Int gridSize_Tiles_BasedOnCells_Maximal = Vector2Int.zero;

    //UI
    bool showGridSizeValues = false;
    bool showGridSizeTilesValues = false;
    bool showGridSizeCellsValues = false;
    bool showGridRowSizesValues = false;
    bool showGridObjects = false;

    //TiledObjects disponibles a chaque positions
    [SerializeField] private PositionsListsAncien availableObjectsByPositions;
    [SerializeField] private List<GridCellWrapper> gridCells = new List<GridCellWrapper>();
    [SerializeField] private List<GridTiledObjectAncien> tiledObjectsWithPosition = new List<GridTiledObjectAncien>();

    public override void Initialize()
    {
        //Grid params initialization
        gridCells.Add(new GridCellWrapper());
        gridCells[0].innerList.Add(new GridCell());

        //Rule Parameters initialization
        //RP_GridSize_TilesX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        //RP_GridSize_TilesY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_CellsX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_CellsY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridRows_Sizes = (RP_GridRowsSizes)ScriptableObjectUtility.CreateAndInitialize<RP_GridRowsSizes>(this);

        rowSizes_Tiles_Minimal = new RowSizes();
        rowSizes_Tiles_Maximal = new RowSizes();

        availableObjectsByPositions = new PositionsListsAncien();

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        //RP_GridSize_TilesX.RemoveNestedParametersAndSelf();
        //RP_GridSize_TilesY.RemoveNestedParametersAndSelf();
        RP_GridSize_CellsX.RemoveNestedParametersAndSelf();
        RP_GridSize_CellsY.RemoveNestedParametersAndSelf();
        RP_GridRows_Sizes.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        //Use options param size or grid parameter size
        Vector2Int desiredSize = options.Length > 0 ? (Vector2Int)options[0] : currentObject.size;
        string selectedSize = options.Length > 0 ? "from option param" : "from currentObject.size";

        Debug.Log("NEW WAY GENERATING NEW GRID. Desired size = " + desiredSize + " " + selectedSize);

        // Pas besoin de verifier par rapport a la taille
        //Verify if the requested size is correct
        //if (!CheckIfAnObjectOfThisSizeCanBeCreated(desiredSize))
        //{
        //    return new BasicTiledObject();
        //}

        GeneratedGrid gg = new GeneratedGrid();
        //gg.heightsAndWidths = GenerateRowsBasedOnDesiredSize(desiredSize);
        gg.sizeCells = GenerateGridSizeCellsFromParameters();
        gg.heightsAndWidths = GenerateRowsBasedOnGridSizeCells(gg.sizeCells);

        BasicTiledObject generatedObject = GenerateAndPlaceGridObjects(gg);

        return generatedObject;
    }

    #region Grid Rows Generation
    private Vector2Int GenerateGridSizeCellsFromParameters()
    {
        Vector2Int generatedSize = new Vector2Int(RP_GridSize_CellsX.GetIntFromParameter(null, null), RP_GridSize_CellsY.GetIntFromParameter(null, null));
        return generatedSize;
    }

    private RowSizes GenerateRowsBasedOnGridSizeCells(Vector2Int gridSizeCells)
    {
        RowSizes generatedRowSizes = new RowSizes();

        Debug.Log("Generating "+ gridSizeCells.x +" Horizontal rows");
        generatedRowSizes.widths = new int[gridSizeCells.x];
        for(int i = 0; i < generatedRowSizes.widths.Length; i ++)
        {
            generatedRowSizes.widths[i] = GetSingleRowSizeBasedOnParameterValues(i, generatedRowSizes.widths.Length, RP_GridRows_Sizes.RP_GridRow_Widths); 
        }

        Debug.Log("Generating " + gridSizeCells.y + " Vertical rows");
        generatedRowSizes.heights = new int[gridSizeCells.y];
        for(int i = 0; i < generatedRowSizes.heights.Length; i ++)
        {
            generatedRowSizes.heights[i] = GetSingleRowSizeBasedOnParameterValues(i, generatedRowSizes.heights.Length, RP_GridRows_Sizes.RP_GridRow_Heights);
        }

        return generatedRowSizes;
    }

    private int GetSingleRowSizeBasedOnParameterValues(int rowPosition, int cellsQuantity, RP_DropDownInt[] rowSizes)
    {
        if (cellsQuantity <= 2)
        {
            if(rowPosition == 0) 
            {
                //First item
                return rowSizes[0].GetIntFromParameter(null, null);
            }
            else
            {
                //Last item
                return rowSizes[2].GetIntFromParameter(null, null);
            }
        }
        else
        {
            if (rowPosition == 0)
            {
                //First item
                return rowSizes[0].GetIntFromParameter(null, null);
            }
            else if (rowPosition == (cellsQuantity - 1))
            {
                //Last item
                return rowSizes[2].GetIntFromParameter(null, null);
            }
            else
            {
                //Middle
                return rowSizes[1].GetIntFromParameter(null, null);
            }
        }
    }

    private BasicTiledObject GenerateAndPlaceGridObjects(GeneratedGrid gg)
    {
        //Prendre des objets au hasard et les placer dans chaque case
        //Vector2Int gridSize_Cells = new Vector2Int(gg.heightsAndWidths.widths.Length, gg.heightsAndWidths.heights.Length);
        Vector2Int currentCell_TilePosition = Vector2Int.zero;
        Vector2Int currentCell_CellPosition = Vector2Int.zero;
        BasicTiledObject finalObject = null;
        object[] sizeOverride = new object[1] { Vector2Int.zero };


        for (int i = 0; i < gg.sizeCells.x; i++)
        {
            if (i > 0)
            {
                currentCell_TilePosition.x += gg.heightsAndWidths.widths[i - 1];
            }
            currentCell_TilePosition.y = 0;
            currentCell_CellPosition.x = i;

            for (int j = 0; j < gg.sizeCells.y; j++)
            {
                if (j > 0)
                {
                    currentCell_TilePosition.y += gg.heightsAndWidths.heights[j - 1];
                }
                currentCell_CellPosition.y = j;


                TiledObject tempTO = availableObjectsByPositions.GetTiledObjectForThisPosition(currentCell_CellPosition, gg.sizeCells);
                if (!tempTO)
                {
                    Debug.LogWarning(this + " These is no Tiled Object for the position " + currentCell_CellPosition);
                    continue;
                }

                sizeOverride[0] = new Vector2Int(gg.heightsAndWidths.widths[i], gg.heightsAndWidths.heights[j]);

                Debug.Log("Generating an object of Size " + sizeOverride[0] + " tiles at position " + currentCell_CellPosition);

                BasicTiledObject tempResult = TiledObjectTools.GetGeneratedObject(tempTO, sizeOverride);

                //Offset tiles position if necessary
                if (currentCell_TilePosition != Vector2Int.zero)
                {
                    for (int h = 0; h < tempResult.positionedTiles.Count; h++)
                    {
                        tempResult.positionedTiles[h].position += currentCell_TilePosition;
                    }
                }

                //Create the first object or add the new ones
                if (finalObject == null)
                {
                    finalObject = tempResult;
                }
                else
                {
                    finalObject.positionedTiles.AddRange(tempResult.positionedTiles);
                }
            }
        }
        return finalObject;
    }
    #endregion

    #region Editor UI
    public override void DisplayParameters()
    {
        showGridSizeValues = EditorGUILayout.Foldout(showGridSizeValues, "Grid Size");
        if (showGridSizeValues)
        {
            EditorGUI.indentLevel++;

            EditorGUI.BeginChangeCheck();

            //DisplayGridSizeTilesParameters();
            DisplayGridSizeCellsParameters();
            DisplayRowSizeParameters();

            if (EditorGUI.EndChangeCheck())
            {
                RefreshGridSizeTilesBasedOnRowSizes();
            }

            EditorGUILayout.Space();

            DisplayGridSizeCalculatedValues();

            EditorGUILayout.Space();

            EditorGUI.indentLevel--;
        }

        showGridObjects = EditorGUILayout.Foldout(showGridObjects, "Tiled Objects");
        if (showGridObjects)
        {
            EditorGUI.BeginChangeCheck();

            DisplayAvailableTiledObjectsList();

            if (EditorGUI.EndChangeCheck())
            {
                //Ancien
                //RefreshObjectPositionsLists();

                //New
                availableObjectsByPositions = PositionsListsAncien.GenerateObjectPositionsLists(tiledObjectsWithPosition);

            }
        }

        EditorUtility.SetDirty(this);
    }

    private void DisplayGridSizeCellsParameters()
    {
        string titleSectionCells = "Size (Cells) : ";
        titleSectionCells += gridSize_Cells_Minimal == gridSize_Cells_Maximal ?
            gridSize_Cells_Minimal.ToString() :
            gridSize_Cells_Minimal + " to " + gridSize_Cells_Maximal;

        showGridSizeCellsValues = EditorGUILayout.Foldout(showGridSizeCellsValues, titleSectionCells);
        if (showGridSizeCellsValues)
        {
            EditorGUILayout.LabelField("Horizontal Size");
            RP_GridSize_CellsX.DisplayParameter();
            EditorGUILayout.LabelField("Vertical Size");
            RP_GridSize_CellsY.DisplayParameter();
        }
    }

    private void DisplayRowSizeParameters()
    {
        string titleSectionRows = "Rows (Tiles) :";

        string widthsTitle = " Widths : ";
        for (int i = 0; i < rowSizes_Tiles_Minimal.widths.Length; i++)
        {
            widthsTitle += rowSizes_Tiles_Minimal.widths[i] == rowSizes_Tiles_Maximal.widths[i] ?
                "| " + rowSizes_Tiles_Minimal.widths[i].ToString() + " |" :
                "| " + rowSizes_Tiles_Minimal.widths[i] + " - " + rowSizes_Tiles_Maximal.widths[i] + " |";
        }

        string heightsTitle = " Heights : ";
        for (int i = 0; i < rowSizes_Tiles_Minimal.heights.Length; i++)
        {
            heightsTitle += rowSizes_Tiles_Minimal.heights[i] == rowSizes_Tiles_Maximal.heights[i] ?
                "| " + rowSizes_Tiles_Minimal.heights[i].ToString() + " |" :
                "| " + rowSizes_Tiles_Minimal.heights[i] + " - " + rowSizes_Tiles_Maximal.heights[i] + " |";
        }

        titleSectionRows += widthsTitle + "," + heightsTitle;

        showGridRowSizesValues = EditorGUILayout.Foldout(showGridRowSizesValues, titleSectionRows, true);
        if (showGridRowSizesValues)
        {
            RP_GridRows_Sizes.DisplayParameter();
        }
    }

    private void DisplayGridSizeCalculatedValues()
    {
        string calculatedSize = gridSize_Tiles_BasedOnCells_Minimal == gridSize_Tiles_BasedOnCells_Maximal ?
            "Calculated grid size (Tiles): " + gridSize_Tiles_BasedOnCells_Minimal :
            "Calculated grid size (Tiles): " + gridSize_Tiles_BasedOnCells_Minimal + " to " + gridSize_Tiles_BasedOnCells_Maximal;

        EditorGUILayout.LabelField(calculatedSize);

        if (gridSize_Tiles_Minimal.x < gridSize_Tiles_BasedOnCells_Minimal.x || gridSize_Tiles_Minimal.y < gridSize_Tiles_BasedOnCells_Minimal.y)
        {
            EditorGUILayout.LabelField("CANNOT GENERATE GRID");

            EditorGUILayout.LabelField("Minimal grid size (Tiles : " + gridSize_Tiles_Minimal + ") is shorter than calculated minimal grid size (" + gridSize_Tiles_BasedOnCells_Minimal + ")");
        }
        if (gridSize_Tiles_Maximal.x > gridSize_Tiles_BasedOnCells_Maximal.x || gridSize_Tiles_Maximal.y > gridSize_Tiles_BasedOnCells_Maximal.y)
        {
            EditorGUILayout.LabelField("CANNOT GENERATE GRID");

            EditorGUILayout.LabelField("Maximal grid size (Tiles) (" + gridSize_Tiles_Maximal + ")  is greater than calculated maximal grid size (Tiles : " + gridSize_Tiles_BasedOnCells_Maximal + ")");
        }
    }

    private void DisplayAvailableTiledObjectsList()
    {
        /* Display the list of available tiled objects for the grid */
        if (tiledObjectsWithPosition.Count == 0)
        {
            if (GUILayout.Button("Add Item"))
            {
                tiledObjectsWithPosition.Add(new GridTiledObjectAncien());
            }
            return;
        }

        for (int i = 0; i < tiledObjectsWithPosition.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Item"))
            {
                tiledObjectsWithPosition.Add(new GridTiledObjectAncien());
            }
            if (GUILayout.Button("Delete Item"))
            {
                tiledObjectsWithPosition.RemoveAt(i);
                EditorGUILayout.EndHorizontal();
                return;
            }

            EditorGUILayout.EndHorizontal();

            tiledObjectsWithPosition[i].tiledObject = (TiledObject)EditorGUILayout.ObjectField("Tiled Object", tiledObjectsWithPosition[i].tiledObject, typeof(TiledObject), false);

            /* Checkboxes corresponding to the available position(s) for this object*/
            List<bool> pos = tiledObjectsWithPosition[i].availablePositions;
            int positionInArrayIndex = 0;
            for (int j = 0; j < 3; j++)
            {
                EditorGUILayout.BeginHorizontal();

                for (int h = 0; h < 3; h++)
                {
                    pos[positionInArrayIndex] = EditorGUILayout.Toggle(pos[positionInArrayIndex]);
                    positionInArrayIndex++;
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
    #endregion

    #region Editor UI functions
    private void RefreshGridSizeTilesBasedOnRowSizes()
    {
        //A SUPPRIMER quand les parametres ne seront plus necessaires
        BasicTiledObject currentObject = new BasicTiledObject();
        object[] options = new object[0];

        //Get grid size in Tiles, based on parameters
        //gridSize_Tiles_Minimal = new Vector2Int(
        //    RP_GridSize_TilesX.GetMinimumIntFromParameter(currentObject, options),
        //    RP_GridSize_TilesY.GetMinimumIntFromParameter(currentObject, options));
        //gridSize_Tiles_Maximal = new Vector2Int(
        //    RP_GridSize_TilesX.GetMaximumIntFromParameter(currentObject, options),
        //    RP_GridSize_TilesY.GetMaximumIntFromParameter(currentObject, options));

        //Get grid size in Cells, based on parameters
        gridSize_Cells_Minimal = new Vector2Int(
            RP_GridSize_CellsX.GetMinimumIntFromParameter(currentObject, options),
            RP_GridSize_CellsY.GetMinimumIntFromParameter(currentObject, options));
        gridSize_Cells_Maximal = new Vector2Int(
            RP_GridSize_CellsX.GetMaximumIntFromParameter(currentObject, options),
            RP_GridSize_CellsY.GetMaximumIntFromParameter(currentObject, options));

        //Get rows sizes based, based on parameters
        rowSizes_Tiles_Minimal.widths = RP_GridRows_Sizes.GetMinimumWidths(currentObject, options);
        rowSizes_Tiles_Minimal.heights = RP_GridRows_Sizes.GetMinimumHeights(currentObject, options);
        rowSizes_Tiles_Maximal.widths = RP_GridRows_Sizes.GetMaximumWidths(currentObject, options);
        rowSizes_Tiles_Maximal.heights = RP_GridRows_Sizes.GetMaximumHeights(currentObject, options);

        //Get grid size in Tiles, based on Cells quantity and sizes
        gridSize_Tiles_BasedOnCells_Minimal = new Vector2Int(
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Minimal.x, rowSizes_Tiles_Minimal.widths),
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Minimal.y, rowSizes_Tiles_Minimal.heights));
        gridSize_Tiles_BasedOnCells_Maximal = new Vector2Int(
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Maximal.x, rowSizes_Tiles_Maximal.widths),
            GetRowLenghtBasedOnRowSizes(gridSize_Cells_Maximal.y, rowSizes_Tiles_Maximal.heights));
    }

    #endregion

    private int GetRowLenghtBasedOnRowSizes(int cellQuantity, int[] columnsOrLinesSizes)
    {
        /* Returns the length (in Tiles) of a row depending on the number of Cells and the size of each row */

        int resultSize = 0;

        for (int i = 0; i < cellQuantity; i++)
        {
            if (i == 0)
            {
                resultSize += columnsOrLinesSizes[0];
            }
            else if (i == cellQuantity - 1)
            {
                resultSize += columnsOrLinesSizes[2];
            }
            else
            {
                resultSize += columnsOrLinesSizes[1];
            }
        }

        return resultSize;
    }

}

