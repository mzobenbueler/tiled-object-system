﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TOR_Modify_Size : TiledObjectRule
{
    [SerializeField] private RP_DropDownInt rp_SizeX;
    [SerializeField] private RP_DropDownInt rp_SizeY;

    public override void Initialize()
    {
        rp_SizeX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        rp_SizeY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        rp_SizeX.RemoveNestedParametersAndSelf();
        rp_SizeY.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        Vector2Int selectedSize = new Vector2Int(rp_SizeX.GetIntFromParameter(currentObject, options), rp_SizeY.GetIntFromParameter(currentObject, options));

        return selectedSize;
    }

    public override void DisplayParameters()
    {
        EditorGUILayout.LabelField("Horizontal Size");
        rp_SizeX.DisplayParameter();

        EditorGUILayout.LabelField("Vertical Size");
        rp_SizeY.DisplayParameter();
    }
}