﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TOR_Modify_RepeatTiledObject : TiledObjectRule
{
    [SerializeField] private RP_RepeaterSimple horizontalRepeat;
    [SerializeField] private RP_RepeaterSimple verticalRepeat;

    public override void Initialize()
    {
        base.Initialize();

        horizontalRepeat = (RP_RepeaterSimple)ScriptableObjectUtility.CreateAndInitialize<RP_RepeaterSimple>(this);
        verticalRepeat = (RP_RepeaterSimple)ScriptableObjectUtility.CreateAndInitialize<RP_RepeaterSimple>(this);

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        horizontalRepeat.RemoveNestedParametersAndSelf();
        verticalRepeat.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        RepeaterParameters horizontalParams = horizontalRepeat.GetRepeaterParameters();
        RepeaterParameters verticalParams = verticalRepeat.GetRepeaterParameters();

        BasicTiledObject horizontalyRepeatedObject = BasicTiledObject.DeepCopy(currentObject);
        horizontalyRepeatedObject.positionedTiles.Clear();

        for (int i = horizontalParams.offset; i < horizontalParams.zoneLenght; i += horizontalParams.spaceBetween)
        {
            //Duplicate the base object 
            BasicTiledObject temp = BasicTiledObject.DeepCopy(currentObject);

            //Change horizontal position of each tiles of the copy of the base
            foreach (PositionedTile pt in temp.positionedTiles)
            {
                //pt.position.x = i;
                pt.position.x = pt.position.x + i;
            }

            //Add the tiles to the result object
            horizontalyRepeatedObject.positionedTiles.AddRange(temp.positionedTiles);
        }

        BasicTiledObject verticalyRepeatedObject = BasicTiledObject.DeepCopy(currentObject);
        verticalyRepeatedObject.positionedTiles.Clear();

        //The object is now duplicated horizontaly. now duplicate it verticaly
        for (int i = verticalParams.offset; i < verticalParams.zoneLenght; i += verticalParams.spaceBetween)
        {
            BasicTiledObject temp = BasicTiledObject.DeepCopy(horizontalyRepeatedObject);

            foreach (PositionedTile pt in temp.positionedTiles)
            {
                //pt.position.y = i;
                pt.position.y = pt.position.y + i;
            }

            //Add these directly in the childBasic object
            verticalyRepeatedObject.positionedTiles.AddRange(temp.positionedTiles);
        }

        return verticalyRepeatedObject;
    }

    public override void DisplayParameters()
    {
        horizontalRepeat.DisplayParameter();
        verticalRepeat.DisplayParameter();
    }
}

[System.Serializable]
public struct RepeaterParameters
{
    public RepeaterParameters(int offset, int zoneLenght, int spaceBetween, int quantity)
    {
        this.offset = offset;
        this.zoneLenght = zoneLenght;
        this.spaceBetween = spaceBetween;
        this.quantity = quantity;
    }

    public int offset;
    public int zoneLenght;
    public int spaceBetween;
    public int quantity;
}

