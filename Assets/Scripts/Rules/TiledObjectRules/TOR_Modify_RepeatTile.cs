﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;
using System;
using System.Reflection;
using System.Net.Http.Headers;

public class TOR_Modify_RepeatTile : TiledObjectRule
{
    [SerializeField] private RP_DropDownInt sizeOptionX;
    [SerializeField] private RP_DropDownInt sizeOptionY;

    public override void Initialize()
    {
        base.Initialize();

        sizeOptionX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        sizeOptionY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        sizeOptionX.RemoveNestedParametersAndSelf();
        sizeOptionY.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        Vector2Int selectedSize = Vector2Int.zero;

        selectedSize.x = sizeOptionX.GetIntFromParameter(currentObject, options);
        selectedSize.y = sizeOptionY.GetIntFromParameter(currentObject, options);

        //This part repeat the tile on the surface. Maybe place it in another parameter ?
        BasicTiledObject result = new BasicTiledObject();
        result.size = selectedSize;
        result.positionedTiles = new List<PositionedTile>();
        for (int i = 0; i < result.size.x; i++)
        {
            for (int j = 0; j < result.size.y; j++)
            {
                result.positionedTiles.Add(new PositionedTile(currentObject.positionedTiles[0].tile, new Vector2Int(i, j)));
            }
        }
        return result;
    }

    public override void DisplayParameters()
    {
        EditorGUILayout.LabelField("Horizontal Size");
        sizeOptionX.DisplayParameter();

        EditorGUILayout.LabelField("Vertical Size");
        sizeOptionY.DisplayParameter();
    }
}