﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TOR_Modify_PositionRelative : TOR_Modify_Position
{
    public POSITION position;

    public override void DeleteSelfAndContainedAssets()
    {
        base.DeleteSelfAndContainedAssets();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        Vector2Int parentTOSize = Vector2Int.zero;

        if (options.Length > 0)
        {
            parentTOSize = (Vector2Int)options[0];
        }
        else
        {
            //parentTOSize = parentBTO.size;
        }

        Vector2Int childTOSize = currentObject.size; 

        Vector2Int basePosition = Vector2Int.zero;
        Vector2Int startDrawingPosition = Vector2Int.zero;

        switch (position)
        {
            case POSITION.TopLeft:
                basePosition = new Vector2Int(0, parentTOSize.y - 1);
                startDrawingPosition = basePosition - new Vector2Int(0, childTOSize.y - 1);
                break;
            case POSITION.Top:
                basePosition = new Vector2Int(1, parentTOSize.y - 1);
                startDrawingPosition = basePosition - new Vector2Int(0, childTOSize.y - 1);
                break;
            case POSITION.TopRight:
                basePosition = new Vector2Int(parentTOSize.x - 1, parentTOSize.y - 1);
                startDrawingPosition = basePosition - new Vector2Int(childTOSize.x - 1, childTOSize.y - 1);
                break;
            case POSITION.Left:
                basePosition = new Vector2Int(0, 1);
                startDrawingPosition = basePosition;
                break;
            case POSITION.Center:
                basePosition = new Vector2Int(1, 1);
                startDrawingPosition = basePosition;
                break;
            case POSITION.Right:
                basePosition = new Vector2Int(parentTOSize.x - 1, 1);
                startDrawingPosition = basePosition - new Vector2Int(childTOSize.x - 1, 0);
                break;
            case POSITION.BottomLeft:
                basePosition = new Vector2Int(0, 0);
                startDrawingPosition = basePosition;
                break;
            case POSITION.Bottom:
                basePosition = new Vector2Int(1, 0);
                startDrawingPosition = basePosition;
                //Set the base position to anywhere in the bottom
                //int xPos = 0;
                //if (parentTOSize.x > 2)
                //{
                //    //Select a random position without selecting an angle
                //    xPos = Random.Range(1, parentTOSize.x - 1);
                //}
                //if (parentTOSize.x == 2)
                //{
                //    Debug.Log("THERE IS ONLY ANGLES TO THE TARGET OBJHECT, NOWHERE TO PLACE A BOTTOM OBJHECT");
                //}
                //basePosition = new Vector2Int(xPos, 0);
                ////There is no need to change the height of the startDrawingPosition
                //startDrawingPosition = basePosition;
                break;
            case POSITION.BottomRight:
                basePosition = new Vector2Int(parentTOSize.x - 1, 0);
                startDrawingPosition = basePosition;
                break;
            default:
                break;
        }

        currentObject = ApplyOffsetToTilePositions(currentObject, startDrawingPosition);
        return currentObject;
    }

    public override void DisplayParameters()
    {
        position = (POSITION)UnityEditor.EditorGUILayout.EnumPopup("Unique Position", position);
    }
}