﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TOR_Add_Contour : TiledObjectRule
{
    [SerializeField] private RP_ContourObjectsList RP_ItemsList;

    public Tile tempTESTContourTile;
    public Tile tempTESTInternTile;


    public override void Initialize()
    {
        RP_ItemsList = (RP_ContourObjectsList)ScriptableObjectUtility.CreateAndInitialize<RP_ContourObjectsList>(this);
       
        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        RP_ItemsList.RemoveNestedParametersAndSelf();
        
        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {


        //Use options param size or grid parameter size
        if(currentObject != null)
        {
            Debug.Log("TOR_Add_Contour - Nombre de Positionned tiles = " + currentObject.positionedTiles.Count);
        }
        else
        {
            Debug.LogWarning("TOR_Add_Contour - Rien en entree");
            return new BasicTiledObject();
        }

        //Prendre les tuiles, recuperer les contours
        int[,] r = ContourRuleTools.ContourGenerator.GenerateContour(currentObject);

        //Generer un objet de contours
        BasicTiledObject generatedObject = ContourRuleTools.BasicTiledObjectGenerator.GenerateBasicTiledObjectFromContours(r, RP_ItemsList.GetContourPositionsListsFromParameter());

        int[,] z = ContourRuleTools.ContourGenerator.CreateTilemapArray(currentObject);

        //Test coloriage
        for (int i = 0; i < z.GetLength(0); i++)
        {
            for (int j = 0; j < z.GetLength(1); j++)
            {
                if(z[i,j] == 1)
                {
                    generatedObject.positionedTiles.Insert(0, new PositionedTile(tempTESTInternTile, new Vector2Int(i, j)));
                }
            }
        }

        return generatedObject;
    }

    public override void DisplayParameters()
    {
        tempTESTContourTile = (Tile)EditorGUILayout.ObjectField(tempTESTContourTile, typeof(Tile), true);
        tempTESTInternTile = (Tile)EditorGUILayout.ObjectField(tempTESTInternTile, typeof(Tile), true);

        RP_ItemsList.DisplayParameter();
    }
}
