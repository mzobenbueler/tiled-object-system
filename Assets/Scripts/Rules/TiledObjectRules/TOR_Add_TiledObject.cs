﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TOR_Add_TiledObject : TiledObjectRule
{
    [SerializeField] private RP_RulesList rp_rule;
    [SerializeField] private RP_TiledObject rp_tiledObject;

    public override void Initialize()
    {
        rp_rule = (RP_RulesList)ScriptableObjectUtility.CreateAndInitialize<RP_RulesList>(this);
        rp_tiledObject = (RP_TiledObject)ScriptableObjectUtility.CreateAndInitialize<RP_TiledObject>(this);
    }

    public override void DeleteSelfAndContainedAssets()
    {
        rp_rule.RemoveNestedParametersAndSelf();
        rp_tiledObject.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        //Use the current object size as parameter to generate the nested object
        options = new object[1] { currentObject.size };

        BasicTiledObject obj = rp_tiledObject.GenerateBasicTiledObjectFromParameter(options);
        List<TiledObjectRule> rules = rp_rule.GetRulesFromParameter();

        obj = TiledObjectTools.ApplyRulesOnBasicObject(obj, rules, options);

        return obj;
    }

    public override void DisplayParameters()
    {
        rp_tiledObject.DisplayParameter();
        rp_rule.DisplayParameter();
    }
}