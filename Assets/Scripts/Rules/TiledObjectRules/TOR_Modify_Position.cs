﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;
using UnityEditor;

public class TOR_Modify_Position : TiledObjectRule
{
    [SerializeField] private RP_DropDownInt positionX;
    [SerializeField] private RP_DropDownInt positionY;

    public override void Initialize()
    {
        positionX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        positionY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        positionX.RemoveNestedParametersAndSelf();
        positionY.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        Vector2Int selectedPosition = new Vector2Int(positionX.GetIntFromParameter(currentObject, options), positionY.GetIntFromParameter(currentObject, options));
       
        currentObject = ApplyOffsetToTilePositions(currentObject, selectedPosition);

        return currentObject;
    }

    protected virtual BasicTiledObject ApplyOffsetToTilePositions(BasicTiledObject childBasicTiledObject, Vector2Int positionsOffset)
    {
        List<PositionedTile> newPositionedTiles = new List<PositionedTile>();

        for (int i = 0; i < childBasicTiledObject.positionedTiles.Count; i++)
        {
            PositionedTile temp = new PositionedTile(childBasicTiledObject.positionedTiles[i].tile, childBasicTiledObject.positionedTiles[i].position + positionsOffset);
            newPositionedTiles.Add(temp);
        }

        childBasicTiledObject.positionedTiles = newPositionedTiles;

        return childBasicTiledObject;
    }

    //public override void DisplayParameters()
    //{
    //    //Display any serialized parameter to show it in Inspector
    //    selectedPosition = UnityEditor.EditorGUILayout.Vector2IntField("Manual Position", selectedPosition);
    //}

    public override void DisplayParameters()
    {
        EditorGUILayout.LabelField("Horizontal Position");
        positionX.DisplayParameter();

        EditorGUILayout.LabelField("Vertical Position");
        positionY.DisplayParameter();
    }
}