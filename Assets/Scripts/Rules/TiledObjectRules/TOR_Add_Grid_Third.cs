﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TOR_Add_Grid_Third : TiledObjectRule
{
    [SerializeField] private RP_DropDownGridGeneration RP_GridGenMethod_Horizontal;
    [SerializeField] private RP_DropDownGridGeneration RP_GridGenMethod_Vertical;

    [SerializeField] private RP_GridLimits RP_Grid_Limits;
    [SerializeField] private RP_GridableObjectsList RP_ItemList;

    public override void Initialize()
    {
        RP_GridGenMethod_Horizontal = (RP_DropDownGridGeneration)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownGridGeneration>(this);
        RP_GridGenMethod_Vertical = (RP_DropDownGridGeneration)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownGridGeneration>(this);

        RP_Grid_Limits = (RP_GridLimits)ScriptableObjectUtility.CreateAndInitialize<RP_GridLimits>(this);
        RP_ItemList = (RP_GridableObjectsList)ScriptableObjectUtility.CreateAndInitialize<RP_GridableObjectsList>(this);

        AssetDatabase.SaveAssets();
    }

    public override void DeleteSelfAndContainedAssets()
    {
        RP_GridGenMethod_Horizontal.RemoveNestedParametersAndSelf();
        RP_GridGenMethod_Vertical.RemoveNestedParametersAndSelf();     
        
        RP_Grid_Limits.RemoveNestedParametersAndSelf();
        RP_ItemList.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override dynamic ApplyRuleOnObject(BasicTiledObject currentObject, params object[] options)
    {
        //Use options param size or grid parameter size
        Vector2Int desiredSize = options.Length > 0 ? (Vector2Int)options[0] : currentObject.size;
        string selectedSize = options.Length > 0 ? "from option param" : "from currentObject.size";

        Debug.Log("Generating Third Grid. Size is " + desiredSize + " " + selectedSize);

        GridLimits limits = RP_Grid_Limits.GetGridLimitsFromParameter();

        GridSideLimitValues gridHorizontalLimits = new GridSideLimitValues(limits.gridSize_Cells_Minimal.x, limits.gridSize_Cells_Maximal.x, limits.rowSizes_Tiles_Minimal.widths, limits.rowSizes_Tiles_Maximal.widths);
        GridSideLimitValues gridVerticalLimits = new GridSideLimitValues(limits.gridSize_Cells_Minimal.y, limits.gridSize_Cells_Maximal.y, limits.rowSizes_Tiles_Minimal.heights, limits.rowSizes_Tiles_Maximal.heights);

        RowSizes rows = new RowSizes();

        Debug.Log("Grid Generation : Horizontal");
        rows.widths = RP_GridGenMethod_Horizontal.GenerateGridSideLengthsFromParameter(desiredSize.x, gridHorizontalLimits);

        Debug.Log("Grid Generation : Vertical");
        rows.heights = RP_GridGenMethod_Vertical.GenerateGridSideLengthsFromParameter(desiredSize.y, gridVerticalLimits);

        BasicTiledObject generatedObject = GridRuleTools.GenerateAndPlaceGridObjects(rows, RP_ItemList.GetGridPositionsListsFromParameter());

        return generatedObject;
    }

    public override void DisplayParameters()
    {
        RP_GridGenMethod_Vertical.DisplayParameter();
        RP_Grid_Limits.DisplayParameter();
        RP_ItemList.DisplayParameter();
    }
}
