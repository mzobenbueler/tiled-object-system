﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ContourRuleTools
{
    public class BasicTiledObjectGenerator
    {
        public static BasicTiledObject GenerateBasicTiledObjectFromContours(int [,] contoursIndexs, ContourPositionsObjects tiles)
        {
            BasicTiledObject result = new BasicTiledObject();
            result.positionedTiles = new List<PositionedTile>();
            result.size = new Vector2Int(contoursIndexs.GetLength(0), contoursIndexs.GetLength(1));

            for (int i = 0; i < contoursIndexs.GetLength(0); i++)
            {
                for (int j = 0; j < contoursIndexs.GetLength(1); j++)
                {
                    if (contoursIndexs[i, j] >= 2) //Place tile on contour
                    {
                        //if (tempTESTContourTile) currentObject.positionedTiles.Add(new PositionedTile(tempTESTContourTile, new Vector2Int(i, j)));
                        RP_ContourObject temp = tiles.GetRandomObjectAvailableForThisContourType(contoursIndexs[i, j]);

                        if (temp)
                        {
                            BasicTiledObject tempBTO = temp.GenerateBasicTiledObjectFromParameter();

                            tempBTO.positionedTiles[0].position = new Vector2Int(i, j);

                            result.positionedTiles.AddRange(tempBTO.positionedTiles);
                        }
                    }
                }
            }

            return result;
        }
    }

    public class ContourGenerator
    {
        public static int[,] GenerateContour(BasicTiledObject obj)
        {
            Debug.Log("Start Generating");

            int[,] r = CreateTilemapArray(obj);

            Vector2Int firstPosition = FindFirstContourPosition(r);

            r = FindContoursFromStartPosition(firstPosition, r);

            int[,] contoursTypes = CreateContoursTypesArray(r);

            return contoursTypes;
        }

        private static int[,] CreateContoursTypesArray (int [,] r)
        {
            int[,] contoursTypes = new int[r.GetLength(0), r.GetLength(1)];

            for (int i = 0; i < contoursTypes.GetLength(0); i++)
            {
                for (int j = 0; j < contoursTypes.GetLength(1); j++)
                {
                    if (r[i, j] == 2)
                    {
                        //Determiner le type de contour
                        contoursTypes[i, j] = DetermineContourType(new Vector2Int(i, j), r);
                    }
                }
            }

            return contoursTypes;
        }

        private static int DetermineContourType(Vector2Int pos, int[,] r)
        {
            //Count "1" values in each angle of the current position
            //Add 2, 4, 8, 16 for each "1" value, depending on the angle, following this scheme

            // 16 |   | 2
            //    |pos|
            //  8 |   | 4

            //Deduce the angle type using the result : 
            // 2, 4, 8, 16 = External angle
            // 6, 12, 24, 18 = Internal angle
            // 14, 28, 26, 22 = Straight line

            Vector2Int[] localPosToScan = new Vector2Int[4]
            {
                //Partir de up, puis sens horaire
                pos + Vector2Int.one,
                pos + Vector2Int.right + Vector2Int.down,
                pos - Vector2Int.one,
                pos + Vector2Int.left + Vector2Int.up
            };

            int oneQuantity = 0;
            bool anAngleIsAlsoContour = false;
            int secondIndex = 0; //To differentiate internal angle and straight line
            for(int i = 0; i < localPosToScan.Length; i ++)
            {
                Vector2Int tempPos = localPosToScan[i];

                //Ignore positions outside the array
                if((tempPos.x < 0 || tempPos.x > r.GetLength(0) - 1) || (tempPos.y < 0 || tempPos.y > r.GetLength(1) - 1))
                {
                    continue;
                }

                if(r[tempPos.x, tempPos.y] != 0)
                {
                    oneQuantity += (int)Mathf.Pow(2, (i + 1));
                }

                if(r[tempPos.x, tempPos.y] == 2)
                {
                    //This is necessary to differentiate an internal contour than a straight lines nearby
                    anAngleIsAlsoContour = true;
                    secondIndex += (int)Mathf.Pow(2, (i + 1));
                }
            }

            Debug.Log("Result " + pos + " = "  + oneQuantity);

            if(anAngleIsAlsoContour)
            {
                if(oneQuantity == 22)
                {
                    switch (secondIndex)
                    {
                        case 2:
                            oneQuantity = 6; //Reste a definir
                            break;
                        case 4:
                            oneQuantity = 18;
                            break;
                        case 8:
                            oneQuantity = 24; //Reste a definir
                            break;
                        case 16:
                            oneQuantity = 6;
                            break;
                    }
                }
                else if (oneQuantity == 14)
                {
                    switch (secondIndex)
                    {
                        case 2:
                            oneQuantity = 12;
                            break;
                        case 4:
                            oneQuantity = 24; //Reste a definir
                            break;
                        case 8:
                            oneQuantity = 6; 
                            break;
                        case 16:
                            oneQuantity = 24; //Reste a definir
                            break;
                    }
                }
                else if (oneQuantity == 28)
                {
                    switch (secondIndex)
                    {
                        case 2:
                            oneQuantity = 6; //Reste a definir 
                            break;
                        case 4:
                            oneQuantity = 24;
                            break;
                        case 8:
                            oneQuantity = 6; //Reste a definir
                            break;
                        case 16:
                            oneQuantity = 12;
                            break;
                    }
                }
                else if (oneQuantity == 26)
                {
                    switch (secondIndex)
                    {
                        case 2:
                            oneQuantity = 24;
                            break;
                        case 4:
                            oneQuantity = 6; //Reste a definir
                            break;
                        case 8:
                            oneQuantity = 18;
                            break;
                        case 16:
                            oneQuantity = 6; //Reste a definir
                            break;
                    }
                }
            }

            switch (oneQuantity)
            {
                case 2:
                    Debug.LogWarning("External Angle : Top Right");
                    break;
                case 4:
                    Debug.LogWarning("External Angle : Bottom Right");
                    break;
                case 8:
                    Debug.LogWarning("External Angle : Bottom Left");
                    break;
                case 16:
                    Debug.LogWarning("External Angle : Top Left");
                    break;

                case 22:
                    Debug.LogWarning("Internal Angle : Top Right");
                    break;            
                case 14:               
                    Debug.LogWarning("Internal Angle : Bottom Right");
                    break;
                case 28:                  
                    Debug.LogWarning("Internal Angle : Bottom Left");
                    break;            
                case 26:              
                    Debug.LogWarning("Internal Angle : Top Left");
                    break;

                case 6:
                    Debug.LogWarning("Straight Line : Right");
                    break;         
                case 12:           
                    Debug.LogWarning("Straight Line : Bottom");
                    break;            
                case 24:              
                    Debug.LogWarning("Straight Line : Left");
                    break;            
                case 18:              
                    Debug.LogWarning("Straight Line : Top");
                    break;

                default:
                    Debug.LogWarning("Incorrect value");
                    break;
            }

            return oneQuantity;
        }



        public static int[,] CreateTilemapArray(BasicTiledObject obj)
        {
            int[,] r = new int[obj.size.x, obj.size.y];

            for (int i = 0; i < r.GetLength(0); i++)
            {
                for (int j = 0; j < r.GetLength(1); j++)
                {
                    r[i, j] = 0;
                }
            }

            foreach (PositionedTile tilePos in obj.positionedTiles)
            {
                r[tilePos.position.x, tilePos.position.y] = 1;
            }

            return r;
        }

        private static Vector2Int FindFirstContourPosition(int[,] r)
        {
            bool firstTileFound = false;

            for (int i = 0; i < r.GetLength(0); i++)
            {
                for (int j = 0; j < r.GetLength(1); j++)
                {
                    //Trouver la premiere case non-vide
                    if (!firstTileFound && !(firstTileFound = HasTile(new Vector2Int(i, j), r)))
                    {
                        continue;
                    }

                    Debug.Log("ContourRuleTool : First tile found = " + new Vector2Int(i, j));
                    return new Vector2Int(i, j);
                }
            }
            return Vector2Int.zero;
        }

        private static int [,] FindContoursFromStartPosition(Vector2Int startPosition, /*List<Vector2Int> posToScan,*/ int[,] r)
        {
            Debug.Log("Follow Contour - Starting to check position at = " + startPosition);

            List<Vector2Int> positionToCheck = new List<Vector2Int>(1) { startPosition };

            while (positionToCheck.Count > 0)
            {
                Debug.Log("Follow Contour - Positions to check count = " + positionToCheck.Count);

                Vector2Int currentPosition = positionToCheck[0];

                Debug.Log("Follow Contour - Now checking " + currentPosition);

                // 1. posToScan is necessarily contour, so set value to = 4
                r[currentPosition.x, currentPosition.y] = 2;
                positionToCheck.RemoveAt(0);

                //Scan every position around
                FindContourNearbyThisPosition(positionToCheck, currentPosition, r);
            }

            return r;

            //The process is 
            // 1. posToScan is necessarily contour, so set value to = 4
            // 2. L'enlever de la liste
            // 2. Scan from posToScan[0] and assign values

            //Array values 
            // 0 = Emtpy 
            // 1 = Need to be checked if nearby an Empty. Else do nothing
            // 2 = Contour confirmed

            //Changer la valeur du pixel actuel

            //Regarder les pixels autour

            //if(posToScan.Count > 0)
            //{
            //    Vector2Int currentPos = posToScan[0];

            //    // 1. posToScan is necessarily contour, so set value to = 4
            //    r[currentPos.x, currentPos.y] = 2;
            //    posToScan.RemoveAt(0);

            //    //Scan every position around
            //    ScanPositions(posToScan, currentPos, r);
            //}
        }

        private static void FindContourNearbyThisPosition(List<Vector2Int> contourToConfirm, Vector2Int currentPos, int[,] r)
        {
            //Array values Reminder
            // 0 = Emtpy 
            // 1 = Need to be checked if nearby is an Empty. Else do nothing
            // 2 = Contour confirmed

            Vector2Int[] nearbyPosToCheck = GetNearbyPositionsToCheck(contourToConfirm, currentPos, r);

            int lastPosIndex = -1;
            Vector2Int lastPosChecked = Vector2Int.zero;

            for(int i = 0; i < nearbyPosToCheck.Length; i ++)
            {
                int currentPosIndex = r[nearbyPosToCheck[i].x, nearbyPosToCheck[i].y];

                //Check if there is a change empty/filled, or filled/empty
                if(currentPosIndex == 0 && lastPosIndex == 1)
                {
                    //The last position must be changed in the array
                    r[lastPosChecked.x, lastPosChecked.y] = 2;

                    //The position must be added to be checked later
                    contourToConfirm.Add(lastPosChecked);
                }
                else if(currentPosIndex == 1 && lastPosIndex == 0)
                {
                    //Same but with the current pos
                    r[nearbyPosToCheck[i].x, nearbyPosToCheck[i].y] = 2;
                    contourToConfirm.Add(nearbyPosToCheck[i]);
                }
                else if(currentPosIndex == 1 && (lastPosIndex == 1 || lastPosIndex == 2)) 
                {
                    // Find Intern angle : Check if the is 
                    if (HasEmptyTileNearby(nearbyPosToCheck[i], r))
                    {
                        //The tile is an angle
                        r[nearbyPosToCheck[i].x, nearbyPosToCheck[i].y] = 2;
                        contourToConfirm.Add(nearbyPosToCheck[i]);
                    }
                }

                lastPosIndex = currentPosIndex;
                lastPosChecked = nearbyPosToCheck[i];
            }
        }

        private static bool HasEmptyTileNearby(Vector2Int positionToCheck, int[,] r)
        {
            Vector2Int[] positionsToCheck = new Vector2Int[4]
            {
                //Partir de up, puis sens horaire
                positionToCheck + Vector2Int.one,
                positionToCheck + Vector2Int.right + Vector2Int.down,
                positionToCheck - Vector2Int.one,
                positionToCheck + Vector2Int.left + Vector2Int.up,
            };

            foreach(Vector2Int pos in positionsToCheck)
            {
                if (pos.x >= 0 && pos.y >= 0 && pos.x <= r.GetLength(0) - 1 && pos.y <= r.GetLength(1) - 1)
                {
                    if(IsEmpty(pos, r))
                    {
                        return true;
                    }
                }  
            }

            return false;
        }

        private static Vector2Int[] GetNearbyPositionsToCheck(List<Vector2Int> contourToConfirm, Vector2Int currentPos, int[,] r)
        {
            //This methods needs to be separated in shorter parts
            //The array should be a list whithout any duplicated values

            Vector2Int[] localPosToScan = new Vector2Int[9]
            {
                //Partir de up, puis sens horaire
                currentPos + Vector2Int.up,
                currentPos + Vector2Int.one,
                currentPos + Vector2Int.right,
                currentPos + Vector2Int.right + Vector2Int.down,
                currentPos + Vector2Int.down,
                currentPos - Vector2Int.one,
                currentPos + Vector2Int.left,
                currentPos + Vector2Int.left + Vector2Int.up,
                currentPos + Vector2Int.up,
            };

            if(currentPos.x == 0 || 
                currentPos.y == 0 || 
                currentPos.x == r.GetLength(0) - 1|| 
                currentPos.y == r.GetLength(1) - 1)
            {
                for (int i = 0; i < localPosToScan.Length; i++)
                {
                    bool hasBeenClamped = false;

                    if (localPosToScan[i].x < 0)
                    {
                        localPosToScan[i].x = 0;
                        hasBeenClamped = true;
                    }
                    else if (localPosToScan[i].x > r.GetLength(0) - 1)
                    {
                        localPosToScan[i].x = r.GetLength(0) - 1;
                        hasBeenClamped = true;
                    }

                    if (localPosToScan[i].y < 0)
                    {
                        localPosToScan[i].y = 0;
                        hasBeenClamped = true;
                    }
                    else if (localPosToScan[i].y > r.GetLength(1) - 1)
                    {
                        localPosToScan[i].y = r.GetLength(1) - 1;
                        hasBeenClamped = true;
                    }

                    if (hasBeenClamped)
                    {
                        //Ajouter la valeur aux trucs si besoin
                        if (r[localPosToScan[i].x, localPosToScan[i].y] == 1)
                        {
                            r[localPosToScan[i].x, localPosToScan[i].y] = 2;
                            contourToConfirm.Add(localPosToScan[i]);
                        }
                    }
                }

            }

            return localPosToScan;
        }

        private static bool HasTile(Vector2Int pos, int[,] r)
        {
            switch (r[pos.x, pos.y])
            {
                default:
                case 0:
                    return false;
                case 1:
                case 2:
                    return true;
            }
        }

        private static bool IsEmpty(Vector2Int pos, int[,] r)
        {
            switch (r[pos.x, pos.y])
            {
                default:
                case 0:
                    return true;
                case 1:
                case 2:
                    return false;
            }
        }




    }
}

[System.Serializable]
public class ContourPositions
{
    public List<bool> externalPositions;
    public List<bool> internalPositions;
    public List<bool> linePositions;

    public ContourPositions()
    {
        externalPositions = new List<bool>() { false, false, false, false };
        internalPositions = new List<bool>() { false, false, false, false };
        linePositions = new List<bool>() { false, false, false, false };
    }
}

[System.Serializable]
public class ContourPositionsObjects
{
    //External Angles
    public List<RP_ContourObject> angleExternalTopLeft;
    public List<RP_ContourObject> angleExternalTopRight;
    public List<RP_ContourObject> angleExternalBottomLeft;
    public List<RP_ContourObject> angleExternalBottomRight;

    //Internal Angles
    public List<RP_ContourObject> angleInternalTopLeft;
    public List<RP_ContourObject> angleInternalTopRight;
    public List<RP_ContourObject> angleInternalBottomLeft;
    public List<RP_ContourObject> angleInternalBottomRight;

    //Lines
    public List<RP_ContourObject> lineTop;
    public List<RP_ContourObject> lineLeft;
    public List<RP_ContourObject> lineRight;
    public List<RP_ContourObject> lineBottom;

    public ContourPositionsObjects()
    {
        angleExternalTopLeft = new List<RP_ContourObject>();
        angleExternalTopRight = new List<RP_ContourObject>();
        angleExternalBottomLeft = new List<RP_ContourObject>();
        angleExternalBottomRight = new List<RP_ContourObject>();

        angleInternalTopLeft = new List<RP_ContourObject>();
        angleInternalTopRight = new List<RP_ContourObject>();
        angleInternalBottomLeft = new List<RP_ContourObject>();
        angleInternalBottomRight = new List<RP_ContourObject>();

        lineTop = new List<RP_ContourObject>();
        lineLeft = new List<RP_ContourObject>();
        lineRight = new List<RP_ContourObject>();
        lineBottom = new List<RP_ContourObject>();
    }

    public static ContourPositionsObjects GenerateObjectPositionsLists(List<RP_ContourObject> gridObjects)
    {
        ContourPositionsObjects newPositions = new ContourPositionsObjects();

        foreach (RP_ContourObject gto in gridObjects)
        {
            //External positions
            for (int i = 0; i < gto.contourPosition.externalPositions.Count; i++)
            {
                if (gto.contourPosition.externalPositions[i] == true)
                {
                    switch (i)
                    {
                        case 0:
                            //TOPLEFT
                            newPositions.angleExternalTopLeft.Add(gto);
                            break;
                        case 1:
                            newPositions.angleExternalTopRight.Add(gto);
                            break;
                        case 2:
                            newPositions.angleExternalBottomLeft.Add(gto);
                            break;
                        case 3:
                            newPositions.angleExternalBottomRight.Add(gto);
                            break;
                    }
                }
            }

            //Internal Positions
            for (int i = 0; i < gto.contourPosition.internalPositions.Count; i++)
            {
                if (gto.contourPosition.internalPositions[i] == true)
                {
                    switch (i)
                    {
                        case 0:
                            //TOPLEFT
                            newPositions.angleInternalTopLeft.Add(gto);
                            break;
                        case 1:
                            newPositions.angleInternalTopRight.Add(gto);
                            break;
                        case 2:
                            newPositions.angleInternalBottomLeft.Add(gto);
                            break;
                        case 3:
                            newPositions.angleInternalBottomRight.Add(gto);
                            break;
                    }
                }
            }

            //Lines
            for (int i = 0; i < gto.contourPosition.linePositions.Count; i++)
            {
                if (gto.contourPosition.linePositions[i] == true)
                {
                    switch (i)
                    {
                        case 0:
                            //TOPLEFT
                            newPositions.lineTop.Add(gto);
                            break;
                        case 1:
                            newPositions.lineBottom.Add(gto);
                            break;
                        case 2:
                            newPositions.lineLeft.Add(gto);
                            break;
                        case 3:
                            newPositions.lineRight.Add(gto);
                            break;
                    }
                }
            }
        }

        return newPositions;
    }

    public RP_ContourObject GetRandomObjectAvailableForThisContourType(int contourTypeIndex)
    {
        return GetRandomTiledObjectFromList(this.GetListOfAvailableObjectForAContourTypeIndex(contourTypeIndex));
    }

    public List<RP_ContourObject> GetListOfAvailableObjectForAContourTypeIndex(int contourTypeIndex)
    {
        switch (contourTypeIndex)
        {
            case 2:
                //Debug.LogWarning("External Angle : Top Right");
                return angleExternalBottomLeft;
            case 4:
                //Debug.LogWarning("External Angle : Bottom Right");
                return angleExternalTopLeft;
            case 8:
                //Debug.LogWarning("External Angle : Bottom Left");
                return angleExternalTopRight;
            case 16:
                //Debug.LogWarning("External Angle : Top Left");
                return angleExternalBottomRight;

            case 22:
                //Debug.LogWarning("Internal Angle : Top Right");
                return angleInternalBottomLeft;
            case 14:
                //Debug.LogWarning("Internal Angle : Bottom Right");
                return angleInternalTopLeft;
            case 28:
                //Debug.LogWarning("Internal Angle : Bottom Left");
                return angleInternalTopRight;
            case 26:
                //Debug.LogWarning("Internal Angle : Top Left");
                return angleInternalBottomRight;

            case 6:
                //Debug.LogWarning("Straight Line : Right");
                return lineLeft;
            case 12:
                //Debug.LogWarning("Straight Line : Bottom");
                return lineTop;
            case 24:
                //Debug.LogWarning("Straight Line : Left");
                return lineRight;
            case 18:
                //Debug.LogWarning("Straight Line : Top");
                return lineBottom;

            default:
                Debug.LogWarning("Incorrect value");
                return null;
        }

    }

    public RP_ContourObject GetRandomObjectAvailableForThisPosition(Vector2Int cellPosition, Vector2Int gridSize)
    {
        return GetRandomTiledObjectFromList(this.GetListOfAvailableObjectsForACell(cellPosition, gridSize));
    }

    private RP_ContourObject GetRandomTiledObjectFromList(List<RP_ContourObject> tiledObjectsList)
    {
        if (tiledObjectsList.Count > 0)
        {
            int selectedIndex = UnityEngine.Random.Range(0, tiledObjectsList.Count);
            return tiledObjectsList[selectedIndex];
        }
        else
        {
            return null;
        }
    }

    private List<RP_ContourObject> GetListOfAvailableObjectsForACell(Vector2Int cellPosition, Vector2Int gridSize)
    {
        return angleExternalTopLeft;

        //if (cellPosition.y == 0)
        //{
        //    if (cellPosition.x == 0)
        //    {
        //        return bottomLeft;
        //    }
        //    else if (cellPosition.x == gridSize.x - 1)
        //    {
        //        return bottomRight;
        //    }
        //    else
        //    {
        //        return bottomCenter;
        //    }
        //}
        //else if (cellPosition.y == gridSize.y - 1)
        //{
        //    if (cellPosition.x == 0)
        //    {
        //        return topLeft;
        //    }
        //    else if (cellPosition.x == gridSize.x - 1)
        //    {
        //        return topRight;
        //    }
        //    else
        //    {
        //        return topCenter;
        //    }
        //}
        //else
        //{
        //    if (cellPosition.x == 0)
        //    {
        //        return middleLeft;
        //    }
        //    else if (cellPosition.x == gridSize.x - 1)
        //    {
        //        return middleRight;
        //    }
        //    else
        //    {
        //        return middleCentre;
        //    }
        //}
    }
}
