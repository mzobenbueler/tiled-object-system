﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridRuleTools : MonoBehaviour
{
    public class GenerationWithMaximumCells
    {
        public static RowSizes GenerateRowsContainingAMaximumOfCells(Vector2Int requestedGridSize_Tiles, GridLimits gridLimits)
        {
            Debug.Log(requestedGridSize_Tiles);

            RowSizes generatedRowSizes = new RowSizes();

            //Creer les row horizontales
            Debug.Log("Generating Horizontal Rows");

            CellsAndTilesQuantities horizontalValues = GetMaximumCellsQuantityAndRestingTiles(
                requestedGridSize_Tiles.x,
                gridLimits.gridSize_Cells_Minimal.x,
                gridLimits.gridSize_Cells_Maximal.x,
                gridLimits.rowSizes_Tiles_Minimal.widths,
                gridLimits.rowSizes_Tiles_Maximal.widths);

            int[] horizontalRows = CreateRowSizesList(horizontalValues.cells, gridLimits.rowSizes_Tiles_Minimal.widths);

            generatedRowSizes.widths = ExpendCellsToFitInGrid(horizontalRows, horizontalValues.tiles, gridLimits.rowSizes_Tiles_Minimal.widths, gridLimits.rowSizes_Tiles_Maximal.widths);

            //Creer les row verticales
            Debug.Log("Generating Vertical Rows");

            CellsAndTilesQuantities verticalValues = GetMaximumCellsQuantityAndRestingTiles(
                requestedGridSize_Tiles.y,
                gridLimits.gridSize_Cells_Minimal.y,
                gridLimits.gridSize_Cells_Maximal.y,
                gridLimits.rowSizes_Tiles_Minimal.heights,
                gridLimits.rowSizes_Tiles_Maximal.heights);

            int[] verticalRowSizes = CreateRowSizesList(verticalValues.cells, gridLimits.rowSizes_Tiles_Minimal.heights);

            generatedRowSizes.heights = ExpendCellsToFitInGrid(verticalRowSizes, verticalValues.tiles, gridLimits.rowSizes_Tiles_Minimal.heights, gridLimits.rowSizes_Tiles_Maximal.heights);

            return generatedRowSizes;
        }

        public static int[] GenerateSingleRowWithMaximumAmountOfCells(int gridLength, GridSideLimitValues sideLimits)
        {
            Debug.Log("Generate Single Row With Maximum Amount Of Cells");

            CellsAndTilesQuantities horizontalValues = GetMaximumCellsQuantityAndRestingTiles(
                gridLength,
                sideLimits.cellsMin,
                sideLimits.cellsMax,
                sideLimits.rowsLengthMin,
                sideLimits.rowsLengthMax);

            int[] horizontalRows = CreateRowSizesList(horizontalValues.cells, sideLimits.rowsLengthMin);

            int[] resultSizes = ExpendCellsToFitInGrid(horizontalRows, horizontalValues.tiles, sideLimits.rowsLengthMin, sideLimits.rowsLengthMax);

            return resultSizes;
        }
    }

    public class GenerationIgnoringDesiredSize
    {
        public static RowSizes GenerateRowsBasedOnGridSizeCells(GridLimits limits)
        {
            RowSizes generatedRowSizes = new RowSizes();

            Vector2Int gridSizeCells = new Vector2Int(
                Random.Range(limits.gridSize_Cells_Minimal.x, limits.gridSize_Cells_Maximal.x),
                Random.Range(limits.gridSize_Cells_Minimal.y, limits.gridSize_Cells_Maximal.y));

            Debug.Log("Generating " + gridSizeCells.x + " Horizontal rows");
            generatedRowSizes.widths = new int[gridSizeCells.x];
            for (int i = 0; i < generatedRowSizes.widths.Length; i++)
            {
                generatedRowSizes.widths[i] = GetSingleRowSizeBasedOnParameterValues(i, generatedRowSizes.widths.Length, limits.rowSizes_Tiles_Minimal.widths, limits.rowSizes_Tiles_Maximal.widths);
            }

            Debug.Log("Generating " + gridSizeCells.y + " Vertical rows");
            generatedRowSizes.heights = new int[gridSizeCells.y];
            for (int i = 0; i < generatedRowSizes.heights.Length; i++)
            {
                generatedRowSizes.heights[i] = GetSingleRowSizeBasedOnParameterValues(i, generatedRowSizes.heights.Length, limits.rowSizes_Tiles_Minimal.heights, limits.rowSizes_Tiles_Maximal.heights);
            }

            return generatedRowSizes;
        }
    }
   
    public static int GetSingleRowSizeBasedOnParameterValues(int rowPosition, int cellsQuantity, int[] min, int[] max)
    {
        if (cellsQuantity <= 2)
        {
            if (rowPosition == 0)
            {
                //First item
                //return rowSizes[0].GetIntFromParameter(null, null);
                return Random.Range(min[0], max[0]);
            }
            else
            {
                //Last item
                //return rowSizes[2].GetIntFromParameter(null, null);
                return Random.Range(min[2], max[2]);
            }
        }
        else
        {
            if (rowPosition == 0)
            {
                //First item
                //return rowSizes[0].GetIntFromParameter(null, null);
                return Random.Range(min[0], max[0]);
            }
            else if (rowPosition == (cellsQuantity - 1))
            {
                //Last item
                //return rowSizes[2].GetIntFromParameter(null, null);
                return Random.Range(min[2], max[2]);
            }
            else
            {
                //Middle
                //return rowSizes[1].GetIntFromParameter(null, null);
                return Random.Range(min[1], max[1]);
            }
        }
    }

    public static CellsAndTilesQuantities GetMaximumCellsQuantityAndRestingTiles(int desiredTilesQuantity, int cellsMinimumQuantity, int cellsMaximumQuantity, int[] currentRow_SizesMin, int[] currentRow_SizesMax)
    {
        //From cellsMin to cellsMax, add cells. Stop when the total size (tiles) is larger that the desired size

        //Start from minimum cells quantity and resulting tiles quantity
        int cellIndex = cellsMinimumQuantity;
        int totalCellsSize = GetTotalRowLenghtBasedOnRowSizes(cellIndex, currentRow_SizesMin);

        while (cellIndex < cellsMaximumQuantity)
        {
            int sizeIfNewCellIsAdded = totalCellsSize + GetSingleRowLenghtBasedOnItsPosition(cellIndex, currentRow_SizesMin);
            if (sizeIfNewCellIsAdded <= desiredTilesQuantity)
            {
                cellIndex++;
                totalCellsSize += GetSingleRowLenghtBasedOnItsPosition(cellIndex, currentRow_SizesMin);
            }
            else
            {
                break;
            }
        };

        CellsAndTilesQuantities result = new CellsAndTilesQuantities();
        result.cells = cellIndex;
        result.tiles = desiredTilesQuantity - totalCellsSize;

        Debug.Log("To reach desired size (" + desiredTilesQuantity + "), need : " + result.cells + " cell(s) (" + totalCellsSize + " tiles), plus " + result.tiles + " tile(s)");

        if (!CheckIfTheObjectCanBeCreatedWithTheseCells(result.cells, desiredTilesQuantity, currentRow_SizesMax))
        {
            Debug.Log("IMPOSSIBLE. Returnng result anyway");
            return result;

            //return null;
        }
        else
        {
            return result;
        }
    }

    public static BasicTiledObject GenerateAndPlaceGridObjects(RowSizes rowSizes, GridPositionsObjects availableObjectsByPositions)
    {
        //Prendre des objets au hasard et les placer dans chaque case
        //Vector2Int gridSize_Cells = new Vector2Int(gg.heightsAndWidths.widths.Length, gg.heightsAndWidths.heights.Length);

        Vector2Int gridCells = new Vector2Int(rowSizes.widths.Length, rowSizes.heights.Length);

        Vector2Int currentCell_TilePosition = Vector2Int.zero;
        Vector2Int currentCell_CellPosition = Vector2Int.zero;
        BasicTiledObject finalObject = null;
        object[] sizeOverride = new object[1] { Vector2Int.zero };

        for (int i = 0; i < gridCells.x; i++)
        {
            if (i > 0)
            {
                currentCell_TilePosition.x += rowSizes.widths[i - 1];
            }
            currentCell_TilePosition.y = 0;
            currentCell_CellPosition.x = i;

            for (int j = 0; j < gridCells.y; j++)
            {
                if (j > 0)
                {
                    currentCell_TilePosition.y += rowSizes.heights[j - 1];
                }
                currentCell_CellPosition.y = j;

                //Ancien
                //TiledObject tempTO = availableObjectsByPositions.GetRandomObjectAvailableForThisPosition(currentCell_CellPosition, gridCells);
                //if (!tempTO)
                //{
                //    Debug.LogWarning(" These is no Tiled Object for the position " + currentCell_CellPosition);
                //    continue;
                //}
                //Debug.Log("Generating an object of Size " + sizeOverride[0] + " tiles at position " + currentCell_CellPosition);
                //BasicTiledObject tempResult = TiledObjectTools.GetGeneratedObject(tempTO, sizeOverride);

                RP_GridableObject objectAtPosition = availableObjectsByPositions.GetRandomObjectAvailableForThisPosition(currentCell_CellPosition, gridCells);

                if (!objectAtPosition)
                {
                    Debug.LogWarning(" These is no Object for the position " + currentCell_CellPosition);
                    continue;
                }

                sizeOverride[0] = new Vector2Int(rowSizes.widths[i], rowSizes.heights[j]);
                BasicTiledObject tempBasicObject = objectAtPosition.RP_GridableObjectType.GenerateBasicTiledObjectFromParameter(sizeOverride);

                //Offset tiles position if necessary
                if (currentCell_TilePosition != Vector2Int.zero)
                {
                    for (int h = 0; h < tempBasicObject.positionedTiles.Count; h++)
                    {
                        tempBasicObject.positionedTiles[h].position += currentCell_TilePosition;
                    }
                }

                //Create the first object or add the new ones
                if (finalObject == null)
                {
                    finalObject = tempBasicObject;
                }
                else
                {
                    finalObject.positionedTiles.AddRange(tempBasicObject.positionedTiles);
                }
            }
        }
        return finalObject;
    }

    public static int GetTotalRowLenghtBasedOnRowSizes(int cellQuantity, int[] columnsOrLinesSizes)
    {
        /* Returns the length (in Tiles) of a row depending on the number of Cells and the size of each row */

        int resultSize = 0;

        for (int i = 0; i < cellQuantity; i++)
        {
            if (i == 0)
            {
                resultSize += columnsOrLinesSizes[0];
            }
            else if (i == cellQuantity - 1)
            {
                resultSize += columnsOrLinesSizes[2];
            }
            else
            {
                resultSize += columnsOrLinesSizes[1];
            }
        }

        return resultSize;
    }

    public static int GetSingleRowLenghtBasedOnItsPosition(int rowPosition, int[] rowSizes)
    {
        if (rowPosition == 0)
        {
            return rowSizes[0];
        }
        else if (rowPosition == 1)
        {
            return rowSizes[2];
        }
        else
        {
            return rowSizes[1];
        }
    }

    public static int[] CreateRowSizesList(int cellQuantity, int[] minimalRowSizes)
    {
        int[] rowSizes = new int[cellQuantity];
        for (int i = 0; i < cellQuantity; i++)
        {
            rowSizes[i] = GetSingleRowLenghtBasedOnItsPosition(i, minimalRowSizes);
        }
        return rowSizes;
    }

    public static int[] ExpendCellsToFitInGrid(int[] rowSizes, int tileQuantityToAdd, int[] rowSizesTilesMin, int[] rowSizesTilesMax)
    {
        //Faire une liste des cells agrandissables 
        List<int> extendableCellsIndexes = new List<int>();
        for (int i = 0; i < rowSizes.Length; i++)
        {
            //Compare minsize and maxsize for this cell
            int currentCellMin = GetSingleRowLenghtBasedOnItsPosition(i, rowSizesTilesMin);
            int currentCellMax = GetSingleRowLenghtBasedOnItsPosition(i, rowSizesTilesMax);

            //Add it the list of enlargeable cells
            if (currentCellMin != currentCellMax)
            {
                Debug.Log("Line " + i + " can be exprended : " + currentCellMin + " to " + currentCellMax);
                extendableCellsIndexes.Add(i);
            }
        }

        //Pour chaque tile manquante, choisir un item au hasard dans cette liste et lui ajouter 1 tile
        for (int i = 0; i < tileQuantityToAdd; i++)
        {
            int randomItemIndex = UnityEngine.Random.Range(0, extendableCellsIndexes.Count);
            int rowToExpendIndex = extendableCellsIndexes[randomItemIndex];

            rowSizes[rowToExpendIndex] += 1;

            //Enlever l'item de la liste si il n'a plus de places
            if (rowSizes[rowToExpendIndex] == GetSingleRowLenghtBasedOnItsPosition(rowSizes[rowToExpendIndex], rowSizesTilesMax))
            {
                extendableCellsIndexes.RemoveAt(randomItemIndex);
            }
        }

        return rowSizes;
    }

    public static bool CheckIfTheObjectCanBeCreatedWithTheseCells(int cellsQuantity, int tileQuantityToReach, int[] currentRow_SizesMax)
    {
        Debug.Log("Checking if the grid can be completed");

        /* Check if it is possible to complete the grid with the maximum sizes. */
        int currentTileQuantity = 0;
        int currentCellIndex = 0;

        while (currentCellIndex < cellsQuantity)
        {
            //Ajouter la taille de la cell actuelle a currenttilequantiyty
            currentTileQuantity += GetSingleRowLenghtBasedOnItsPosition(currentCellIndex, currentRow_SizesMax);
            currentCellIndex++;
        };

        if (currentTileQuantity >= tileQuantityToReach)
        {
            return true;
        }
        else
        {
            Debug.LogWarning("Cannot reach desired size (" + tileQuantityToReach + ")");
            return false;
        }
    }
}

public struct GridSideLimitValues
{
    public int cellsMin;
    public int cellsMax;
    public int[] rowsLengthMin;
    public int[] rowsLengthMax;

    public GridSideLimitValues(int cellsMin, int cellsMax, int[] rowsLengthMin, int[] rowsLengthMax)
    {
        this.cellsMin = cellsMin;
        this.cellsMax = cellsMax;
        this.rowsLengthMin = (int[])rowsLengthMin.Clone();
        this.rowsLengthMax = (int[])rowsLengthMax.Clone();
    }
}

public class CellsAndTilesQuantities
{
    public int cells;
    public int tiles;
}

[System.Serializable]
public class GeneratedGrid
{
    public Vector2Int sizeTiles;
    public Vector2Int sizeCells;
    public RowSizes heightsAndWidths;
    public List<List<TiledObject>> tiledObjectsGrid;

    public GeneratedGrid()
    {
        heightsAndWidths = new RowSizes();
    }
}

[System.Serializable]
public class GridLimits
{
    //Grid size by Tiles
    public Vector2Int gridSize_Tiles_Minimal = Vector2Int.one;
    public Vector2Int gridSize_Tiles_Maximal = Vector2Int.one;

    //Grid size by Cells
    public Vector2Int gridSize_Cells_Minimal = Vector2Int.one;
    public Vector2Int gridSize_Cells_Maximal = Vector2Int.one;

    //Based on RP_GridRows_Sizes
    public RowSizes rowSizes_Tiles_Minimal;
    public RowSizes rowSizes_Tiles_Maximal;

    public GridLimits()
    {
        rowSizes_Tiles_Minimal = new RowSizes();
        rowSizes_Tiles_Maximal = new RowSizes();
    }


    public void UpdateTilesLimits(Vector2Int min, Vector2Int max)
    {
        gridSize_Tiles_Minimal = min;
        gridSize_Tiles_Maximal = max;
    }

    public void UpdateCellsLimits(Vector2Int min, Vector2Int max)
    {
        gridSize_Cells_Minimal = min;
        gridSize_Cells_Maximal = max;
    }

    public void UpdatesRowsLimits(int[] widthsMin, int[] widthsMax, int[] heightsMin, int[]heightsMax)
    {
        rowSizes_Tiles_Minimal.widths = widthsMin;
        rowSizes_Tiles_Maximal.widths = widthsMax;

        rowSizes_Tiles_Minimal.heights = heightsMin;
        rowSizes_Tiles_Maximal.heights = heightsMax;
    }
}

[System.Serializable]
public class RowSizes
{
    public int[] heights;
    public int[] widths;

    public RowSizes()
    {
        heights = new int[3] { 1, 1, 1 };
        widths = new int[3] { 1, 1, 1 };
    }
}

[System.Serializable]
public class GridCellWrapper
{
    public List<GridCell> innerList;

    public GridCellWrapper()
    {
        innerList = new List<GridCell>();
    }

    public GridCellWrapper(int initSize)
    {
        innerList = new List<GridCell>();

        for (int i = 0; i < initSize; ++i)
        {
            innerList.Add(new GridCell());
        }
    }
}


[System.Serializable]
public class GridCell
{
    public TiledObject tiledObject;
    public Vector2Int cellSizeMin;
    public Vector2Int cellSizeMax;
    public Vector2Int cellPosition;
    public Vector2Int cellCurrentSize;
}

[System.Serializable]
public class GridTiledObjectAncien
{
    public GridTiledObjectAncien()
    {
        availablePositions = new List<bool>()
        {
            false, false, false,
            false, false, false,
            false, false, false
        };
    }

    public TiledObject tiledObject;
    public List<bool> availablePositions;
}

[System.Serializable]
public class GridPositions
{
    public GridPositions()
    {
        activePositions = new List<bool>()
        {
            false, false, false,
            false, false, false,
            false, false, false
        };
    }

    public List<bool> activePositions;
}

[System.Serializable]
public class Vector2IntListWrapper
{
    public List<Vector2Int> innerList;

    public Vector2IntListWrapper()
    {
        innerList = new List<Vector2Int>();
    }

    public Vector2IntListWrapper(int initSize)
    {
        innerList = new List<Vector2Int>();

        for (int i = 0; i < initSize; ++i)
        {
            innerList.Add(Vector2Int.zero);
        }
    }
}

//A supprimer en meme temps que les vieilles classes de GRID
[System.Serializable]
public class PositionsListsAncien
{
    public List<TiledObject> topLeft;
    public List<TiledObject> topCenter;
    public List<TiledObject> topRight;

    public List<TiledObject> middleLeft;
    public List<TiledObject> middleCentre;
    public List<TiledObject> middleRight;

    public List<TiledObject> bottomLeft;
    public List<TiledObject> bottomCenter;
    public List<TiledObject> bottomRight;

    public PositionsListsAncien()
    {
        topLeft = new List<TiledObject>();
        topCenter = new List<TiledObject>();
        topRight = new List<TiledObject>();

        middleLeft = new List<TiledObject>();
        middleCentre = new List<TiledObject>();
        middleRight = new List<TiledObject>();

        bottomLeft = new List<TiledObject>();
        bottomCenter = new List<TiledObject>();
        bottomRight = new List<TiledObject>();
    }

    public static PositionsListsAncien GenerateObjectPositionsLists(List<GridTiledObjectAncien> tiledObjectsWithPosition)
    {
        PositionsListsAncien newList = new PositionsListsAncien();

        foreach (GridTiledObjectAncien gto in tiledObjectsWithPosition)
        {
            for (int i = 0; i < gto.availablePositions.Count; i++)
            {
                if (gto.availablePositions[i] == true)
                {
                    switch (i)
                    {
                        case 0:
                            //TOPLEFT
                            newList.topLeft.Add(gto.tiledObject);
                            break;
                        case 1:
                            newList.topCenter.Add(gto.tiledObject);
                            break;
                        case 2:
                            newList.topRight.Add(gto.tiledObject);
                            break;
                        case 3:
                            newList.middleLeft.Add(gto.tiledObject);
                            break;
                        case 4:
                            newList.middleCentre.Add(gto.tiledObject);
                            break;
                        case 5:
                            newList.middleRight.Add(gto.tiledObject);
                            break;
                        case 6:
                            newList.bottomLeft.Add(gto.tiledObject);
                            break;
                        case 7:
                            newList.bottomCenter.Add(gto.tiledObject);
                            break;
                        case 8:
                            newList.bottomRight.Add(gto.tiledObject);
                            break;
                    }
                }
            }
        }

        return newList;
    }

    public TiledObject GetTiledObjectForThisPosition(Vector2Int cellPosition, Vector2Int gridSize)
    {
        return GetRandomTiledObjectFromList(this.GetListOfAvailableObjectsForACell(cellPosition, gridSize));
    }

    private TiledObject GetRandomTiledObjectFromList(List<TiledObject> tiledObjectsList)
    {
        if (tiledObjectsList.Count > 0)
        {
            int selectedIndex = UnityEngine.Random.Range(0, tiledObjectsList.Count);
            return tiledObjectsList[selectedIndex];
        }
        else
        {
            return null;
        }
    }

    private List<TiledObject> GetListOfAvailableObjectsForACell(Vector2Int cellPosition, Vector2Int gridSize)
    {
        if (cellPosition.y == 0)
        {
            if (cellPosition.x == 0) 
            {
                return bottomLeft;
            }
            else if (cellPosition.x == gridSize.x - 1)
            {
                return bottomRight;
            }
            else
            {
                return bottomCenter;
            }
        }
        else if (cellPosition.y == gridSize.y - 1) 
        {
            if (cellPosition.x == 0)
            {
                return topLeft;
            }
            else if (cellPosition.x == gridSize.x - 1)
            {
                return topRight;
            }
            else
            {
                return topCenter;
            }
        }
        else
        {
            if (cellPosition.x == 0) 
            {
                return middleLeft;
            }
            else if (cellPosition.x == gridSize.x - 1)
            {
                return middleRight;
            }
            else
            {
                return middleCentre;
            }
        }
    }
}

[System.Serializable]
public class GridPositionsObjects
{
    public List<RP_GridableObject> topLeft;
    public List<RP_GridableObject> topCenter;
    public List<RP_GridableObject> topRight;
                
    public List<RP_GridableObject> middleLeft;
    public List<RP_GridableObject> middleCentre;
    public List<RP_GridableObject> middleRight;
                
    public List<RP_GridableObject> bottomLeft;
    public List<RP_GridableObject> bottomCenter;
    public List<RP_GridableObject> bottomRight;

    public GridPositionsObjects()
    {
        topLeft = new List<RP_GridableObject>();
        topCenter = new List<RP_GridableObject>();
        topRight = new List<RP_GridableObject>();

        middleLeft = new List<RP_GridableObject>();
        middleCentre = new List<RP_GridableObject>();
        middleRight = new List<RP_GridableObject>();

        bottomLeft = new List<RP_GridableObject>();
        bottomCenter = new List<RP_GridableObject>();
        bottomRight = new List<RP_GridableObject>();
    }

    public static GridPositionsObjects GenerateObjectPositionsLists(List<RP_GridableObject> gridObjects)
    {
        GridPositionsObjects newList = new GridPositionsObjects();

        foreach (RP_GridableObject gto in gridObjects)
        {
            for (int i = 0; i < gto.gridPositions.activePositions.Count; i++)
            {
                if (gto.gridPositions.activePositions[i] == true)
                {
                    switch (i)
                    {
                        case 0:
                            //TOPLEFT
                            newList.topLeft.Add(gto);
                            break;
                        case 1:
                            newList.topCenter.Add(gto);
                            break;
                        case 2:
                            newList.topRight.Add(gto);
                            break;
                        case 3:
                            newList.middleLeft.Add(gto);
                            break;
                        case 4:
                            newList.middleCentre.Add(gto);
                            break;
                        case 5:
                            newList.middleRight.Add(gto);
                            break;
                        case 6:
                            newList.bottomLeft.Add(gto);
                            break;
                        case 7:
                            newList.bottomCenter.Add(gto);
                            break;
                        case 8:
                            newList.bottomRight.Add(gto);
                            break;
                    }
                }
            }
        }

        return newList;
    }

    public RP_GridableObject GetRandomObjectAvailableForThisPosition(Vector2Int cellPosition, Vector2Int gridSize)
    {
        return GetRandomTiledObjectFromList(this.GetListOfAvailableObjectsForACell(cellPosition, gridSize));
    }

    private RP_GridableObject GetRandomTiledObjectFromList(List<RP_GridableObject> tiledObjectsList)
    {
        if (tiledObjectsList.Count > 0)
        {
            int selectedIndex = UnityEngine.Random.Range(0, tiledObjectsList.Count);
            return tiledObjectsList[selectedIndex];
        }
        else
        {
            return null;
        }
    }

    private List<RP_GridableObject> GetListOfAvailableObjectsForACell(Vector2Int cellPosition, Vector2Int gridSize)
    {
        if (cellPosition.y == 0)
        {
            if (cellPosition.x == 0)
            {
                return bottomLeft;
            }
            else if (cellPosition.x == gridSize.x - 1)
            {
                return bottomRight;
            }
            else
            {
                return bottomCenter;
            }
        }
        else if (cellPosition.y == gridSize.y - 1)
        {
            if (cellPosition.x == 0)
            {
                return topLeft;
            }
            else if (cellPosition.x == gridSize.x - 1)
            {
                return topRight;
            }
            else
            {
                return topCenter;
            }
        }
        else
        {
            if (cellPosition.x == 0)
            {
                return middleLeft;
            }
            else if (cellPosition.x == gridSize.x - 1)
            {
                return middleRight;
            }
            else
            {
                return middleCentre;
            }
        }
    }
}