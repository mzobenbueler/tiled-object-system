﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public abstract class RuleParameter : ScriptableObject
{
    public ScriptableObject mainAsset;

    [SerializeField] protected string foldoutTitle = "";
    [SerializeField] protected bool showRule = false;

    public virtual int GetIntFromParameter(BasicTiledObject currentObject, params object[] options)
    {
        //Debug.Log("GetIntFromParameter not implemented in " + this);
        return 0;
    }

    public virtual int ModifyIntFromParameter(int valueToModify)
    {
        return valueToModify;
    }

    public virtual int GetMinimumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        //int minValue = 0;
        return 0;
    }

    public virtual int GetMaximumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        //int maxValue = 0;
        return 0;
    }

    public virtual void DisplayParameter()
    {
        return;
    }

    public virtual void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        //Debug.LogWarning("Parameter Initialization not implemented in " + this);
        return;
    }

    public virtual void RemoveNestedParametersAndSelf()
    {
        //Debug.LogWarning("Remove Contained Parameter not implemented in " + this);
        //AssetDatabase.RemoveObjectFromAsset(this);
        return;
    }

    public virtual RowSizes GenerateGridFromParameter(Vector2Int requestedGridSize_Tiles, GridLimits gridLimits)
    {
        return new RowSizes();
    }

    public virtual int[] GenerateGridSideLengthsFromParameter(int gridLength, GridSideLimitValues sideLimitsValues)
    {
        return new int[0];
    }

    public virtual GridLimits GetGridLimitsFromParameter()
    {
        return new GridLimits();
    }

    //public virtual PositionsListsAncien GetPositionsListsFromParameter()
    //{
    //    return new PositionsListsAncien();
    //}

    public virtual GridPositionsObjects GetGridPositionsListsFromParameter()
    {
        return new GridPositionsObjects();
    }

    public virtual ContourPositionsObjects GetContourPositionsListsFromParameter()
    {
        return new ContourPositionsObjects();
    }

    public virtual BasicTiledObject GenerateBasicTiledObjectFromParameter(params object[] options)
    {
        return new BasicTiledObject();
    }
}
