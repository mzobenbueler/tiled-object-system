﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class RP_IntBasedOnParentValues : RuleParameter
{
    public RP_DropDownParentSize parentSizeOptions;
    public RP_DropDownIncludedBorders borderOptions;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        parentSizeOptions = (RP_DropDownParentSize)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownParentSize>(assetToSaveIn);

        borderOptions = (RP_DropDownIncludedBorders)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownIncludedBorders>(assetToSaveIn);

        AssetDatabase.SaveAssets();
    }

    public override int GetIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        int selectedParentValue;
        int finalValue;

        selectedParentValue = parentSizeOptions.GetIntFromParameter(currentObject, options);
        finalValue = borderOptions.ModifyIntFromParameter(selectedParentValue);

        return finalValue; 
    }

    public override void DisplayParameter()
    {
        EditorGUILayout.BeginHorizontal();
        parentSizeOptions.DisplayParameter();
        borderOptions.DisplayParameter();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        parentSizeOptions.RemoveNestedParametersAndSelf();
        borderOptions.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(parentSizeOptions);
        AssetDatabase.RemoveObjectFromAsset(borderOptions);
    }
}
