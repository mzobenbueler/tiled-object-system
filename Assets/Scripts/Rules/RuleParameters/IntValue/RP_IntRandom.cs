﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_IntRandom : RuleParameter
{
    [SerializeField] private RP_DropDownInt dropDownIntMin;
    [SerializeField] private RP_DropDownInt dropDownIntMax;

    [SerializeField] private bool minFoldout = false;
    [SerializeField] private bool maxFoldout = false;
    [SerializeField] private string minFoldoutTitle = "Minimum Value";
    [SerializeField] private string maxFoldoutTitle = "Maximum Value";

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        dropDownIntMin = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);
        dropDownIntMax = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);

        AssetDatabase.SaveAssets();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        dropDownIntMin.RemoveNestedParametersAndSelf();
        dropDownIntMax.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(dropDownIntMin);
        AssetDatabase.RemoveObjectFromAsset(dropDownIntMax);
    }

    public override void DisplayParameter()
    {
        EditorGUI.indentLevel++;

        minFoldout = EditorGUILayout.Foldout(minFoldout, minFoldoutTitle, true);
        if (minFoldout)
        {
            dropDownIntMin.DisplayParameter();
        }

        maxFoldout = EditorGUILayout.Foldout(maxFoldout, maxFoldoutTitle, true);
        if (maxFoldout)
        {
            dropDownIntMax.DisplayParameter();
        }

        EditorGUI.indentLevel--;

        EditorGUILayout.Space();
    }

    public override int GetIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        int minimumInt = dropDownIntMin.GetIntFromParameter(currentObject, options);
        int maximumInt = dropDownIntMax.GetIntFromParameter(currentObject, options);

        int finalInt = Random.Range(minimumInt, maximumInt);
        return finalInt;
    }

    public override int GetMinimumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return dropDownIntMin.GetIntFromParameter(currentObject, options);
    }

    public override int GetMaximumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return dropDownIntMax.GetIntFromParameter(currentObject, options);
    }
}