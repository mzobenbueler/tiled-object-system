﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;

[System.Serializable]
public class RP_IntManual : RuleParameter
{
    public int manualSizeValue = 0; 

    public override void DisplayParameter()
    {
        manualSizeValue = EditorGUILayout.IntField("Value", manualSizeValue, GUILayout.ExpandWidth(true));
    }
    public override int GetIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return manualSizeValue;
    }

    public override int GetMinimumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return manualSizeValue;
    }

    public override int GetMaximumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return manualSizeValue;
    }
}
