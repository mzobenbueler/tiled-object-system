﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_RulesList : RuleParameter
{
    [SerializeField] private List<TiledObjectRule> rules;
    [SerializeField] private List<bool> foldouts;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        rules = new List<TiledObjectRule>();
        foldouts = new List<bool>();
        mainAsset = assetToSaveIn;
        AssetDatabase.SaveAssets();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        foreach (TiledObjectRule rule in rules)
        {
            rule.DeleteSelfAndContainedAssets();
        }

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public List<TiledObjectRule> GetRulesFromParameter()
    {
        return rules;
    }

    public override void DisplayParameter()
    {
        if(rules.Count != foldouts.Count)
        {
            foldouts = new List<bool>();
            foreach(TiledObjectRule r in rules)
            {
                foldouts.Add(true);
            }
        }

        foldoutTitle = "Rules (" + rules.Count + ")";
        showRule = EditorGUILayout.Foldout(showRule, foldoutTitle, true);
        if (!showRule)
        {
            return;
        }

        //Create item if the list is emtpy
        if (rules.Count == 0)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Add a rule to this object");
            DisplayListActionButtons(0, 0);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            return;
        }

        //For each item of the list 
        for (int i = 0; i < rules.Count; i++)
        {
            int listSize = rules.Count;

            EditorGUI.indentLevel++;

            EditorGUILayout.BeginHorizontal();

            //if (rules.Count == 0)
            //{
            //    return; //Avoid warning if the list is emptied using an Action Button
            //}

            foldouts[i] = EditorGUILayout.Foldout(foldouts[i], "Rule " + (i + 1), true);
            rules[i] = TiledObjectTools.DisplayDropDown(rules[i], null, mainAsset);
            DisplayListActionButtons(i, rules.Count);

            if(listSize != rules.Count)
            {
                return;
            }



            EditorGUILayout.EndHorizontal();

            if (foldouts[i])
            {
                EditorGUI.indentLevel++;
                if (rules[i])
                {
                    rules[i].DisplayParameters();
                }
                EditorGUI.indentLevel--;
            }

            EditorGUI.indentLevel--;

            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();
        EditorUtility.SetDirty(this);
    }

    private void DisplayListActionButtons(int index, int listSize)
    {
        bool bCanDelete = listSize >= 1;
        bool bCanMoveUp = index > 0;
        bool bCanMoveDown = index < listSize - 1;

        EditorGUILayout.BeginHorizontal(GUILayout.Width(200), GUILayout.ExpandWidth(false));

        if (GUILayout.Button("Add Rule"))
        {
            int insertIndex = listSize == 0 ? 0 : 1; 

            rules.Insert(index + insertIndex, null);
            foldouts.Insert(index + insertIndex, true);
        }

        GUI.enabled = bCanDelete;
        if (GUILayout.Button("Delete"))
        {
            rules[index].DeleteSelfAndContainedAssets();
            rules.RemoveAt(index);
            foldouts.RemoveAt(index);
        }

        GUI.enabled = bCanMoveUp;
        if (GUILayout.Button("Up"))
        {
            TiledObjectRule temp = rules[index];
            rules[index] = rules[index - 1];
            rules[index - 1] = temp;

            bool tempBool = foldouts[index];
            foldouts[index] = foldouts[index - 1];
            foldouts[index - 1] = tempBool;
        }

        GUI.enabled = bCanMoveDown;
        if (GUILayout.Button("Down"))
        {
            TiledObjectRule temp = rules[index];
            rules[index] = rules[index + 1];
            rules[index + 1] = temp;

            bool tempBool = foldouts[index];
            foldouts[index] = foldouts[index + 1];
            foldouts[index + 1] = tempBool;
        }

        GUI.enabled = true;

        EditorGUILayout.EndHorizontal();
    }
}
