﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class RP_DropDownInt : RP_DropDown
{
    public override void PopulateAvailableParametersTypesAndLabels()
    {
        availableParameterTypes = new Type[3]
        {
            typeof(RP_IntManual),
            typeof(RP_IntBasedOnParentValues),
            typeof(RP_IntRandom)
        };

        availableParameterLabels = new string[3]
        {
            "Manual",
            "Based on Parent Size",
            "Random Value"
        };
    }
}

// //BACKUP
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using UnityEditor;
//using UnityEngine;

//[System.Serializable]
//public class RP_SizeOption : RuleParameter
//{
//    Type[] availableParameterTypes = new Type[2]
//    {
//        typeof(RP_SizeOption_Manual),
//        typeof(RP_SizeOption_BasedOnParentSize)
//    };

//    string[] availableParameterLabels = new string[2]
//    {
//        "Manual",
//        "Based on Parent Size"
//    };

//    public int selectedParameterIndex = 0;
//    public RuleParameter selectedParameter;

//    public override void InitializeParameters(ScriptableObject assetToSaveIn)
//    {
//        UnityEngine.Debug.Log("initialization des parametres contenus dans " + this);

//        selectedParameter = ScriptableObjectUtility.CreateAndInitialize(availableParameterTypes[0], assetToSaveIn) as RuleParameter;

//        var dependencis = AssetDatabase.GetDependencies(AssetDatabase.GetAssetPath(this));
//    }

//    public override void DisplayParameter()
//    {
//        EditorGUILayout.LabelField("Size Option");

//        selectedParameterIndex = EditorGUILayout.Popup(selectedParameterIndex, availableParameterLabels);

//        if (availableParameterTypes[selectedParameterIndex] != selectedParameter.GetType())
//        {
//            //Si le parametre selectionne a change
//            ChangeSelectedParameter();
//        }

//        selectedParameter.DisplayParameter();
//    }

//    //valable juste pour les dropdown
//    //Peut etre creer une class expres ?
//    public void ChangeSelectedParameter()
//    {
//        UnityEngine.Debug.Log(AssetDatabase.GetAssetPath(this));

//        //Remove the selected one
//        RemoveContainedParameters();

//        //Create a new one 
//        selectedParameter = ScriptableObjectUtility.CreateAndInitialize(availableParameterTypes[selectedParameterIndex], mainAsset) as RuleParameter;

//        AssetDatabase.SaveAssets();
//        AssetDatabase.Refresh();
//    }

//    public override void RemoveContainedParameters()
//    {
//        base.RemoveContainedParameters();

//        //Delete contained Parameters
//        selectedParameter.RemoveContainedParameters();

//        AssetDatabase.RemoveObjectFromAsset(selectedParameter);
//    }
//}
