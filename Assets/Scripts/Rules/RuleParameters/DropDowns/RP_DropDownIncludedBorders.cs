﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class RP_DropDownIncludedBorders : RP_DropDown
{
    public override void PopulateAvailableParametersTypesAndLabels()
    {
        availableParameterTypes = new Type[3]
        {
            typeof(RP_ModifyIntIncludeBorders),
            typeof(RP_ModifyIntMinusOneBorder),
            typeof(RP_ModifyIntMinusTwoBorders)
        };

        availableParameterLabels = new string[3]
        {
            "Include Borders",
            "Minus One Border",
            "Minus Two Border"
        };
    }
}
