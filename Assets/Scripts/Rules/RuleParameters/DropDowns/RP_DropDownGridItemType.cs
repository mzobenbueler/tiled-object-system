﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RP_DropDownGridItemType : RP_DropDown
{
    public override void PopulateAvailableParametersTypesAndLabels()
    {
        availableParameterTypes = new Type[2]
        {
            typeof(RP_Tile),
            typeof(RP_TiledObject),
        };

        availableParameterLabels = new string[2]
        {
            "Simple Tile",
            "Tiled Object"
        };
    }
}
