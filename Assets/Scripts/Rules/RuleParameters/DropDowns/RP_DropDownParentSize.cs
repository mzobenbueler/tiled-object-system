﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_DropDownParentSize : RP_DropDown
{
    public override void PopulateAvailableParametersTypesAndLabels()
    {
        availableParameterTypes = new Type[2]
        {
            typeof(RP_ParentSizeWidth),
            typeof(RP_ParentSizeHeight)
        };

        availableParameterLabels = new string[2]
        {
            "Parent Width",
            "Parent Height"
        };
    }
}
