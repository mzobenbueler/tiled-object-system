﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RP_DropDownGridGeneration : RP_DropDown
{
    public override void PopulateAvailableParametersTypesAndLabels()
    {
        availableParameterTypes = new Type[2]
        {
            typeof(RP_GridGeneration_BasedOnTiles),
            typeof(RP_GridGeneration_BasedOnCells),
        };

        availableParameterLabels = new string[2]
        {
            "Based on grid size",
            "Based on grid cells"
        };
    }
}
