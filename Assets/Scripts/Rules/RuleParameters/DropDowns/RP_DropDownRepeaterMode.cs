﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_DropDownRepeaterMode : RP_DropDown
{
    public override void PopulateAvailableParametersTypesAndLabels()
    {
        availableParameterTypes = new Type[2]
        {
            typeof(RP_RepeaterMode_SizeBased),
            typeof(RP_RepeaterMode_QuantityBased)
        };

        availableParameterLabels = new string[2]
        {
            "Based on Size",
            "Based on Quantity"
        };
    }
}
