﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class RP_DropDown : RuleParameter
{
    [SerializeField] protected RuleParameter selectedParameter;
    [SerializeField] protected int selectedParameterIndex = 0;

    [SerializeField] protected Type[] availableParameterTypes = new Type[0];
    [SerializeField] protected string[] availableParameterLabels;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        PopulateAvailableParametersTypesAndLabels();

        selectedParameter = ScriptableObjectUtility.CreateAndInitialize(availableParameterTypes[0], assetToSaveIn) as RuleParameter;
    }

    public override void RemoveNestedParametersAndSelf()
    {
        selectedParameter.RemoveNestedParametersAndSelf();
        AssetDatabase.RemoveObjectFromAsset(selectedParameter);

        AssetDatabase.RemoveObjectFromAsset(this);
    }

    public virtual void PopulateAvailableParametersTypesAndLabels()
    {
        Debug.LogWarning("DropDown information not implemented in " + this);
    }

    public override void DisplayParameter()
    {        
        //This is mandatory because the array of Types cannot be Serialized
        if (availableParameterTypes.Length == 0)
        {
            PopulateAvailableParametersTypesAndLabels();
        }

        EditorGUI.indentLevel += 1;

        EditorGUI.BeginChangeCheck();

        selectedParameterIndex = EditorGUILayout.Popup(selectedParameterIndex, availableParameterLabels);

        if (EditorGUI.EndChangeCheck())
        {
            ReplaceParameter();
        }

        selectedParameter.DisplayParameter();

        EditorGUI.indentLevel -= 1;
    }

    private void ReplaceParameter()
    {
        //Remove old parameter
        selectedParameter.RemoveNestedParametersAndSelf();
        AssetDatabase.RemoveObjectFromAsset(selectedParameter);

        //Create new parameter
        selectedParameter = ScriptableObjectUtility.CreateAndInitialize(availableParameterTypes[selectedParameterIndex], mainAsset) as RuleParameter;
    }

    public override int GetIntFromParameter(BasicTiledObject currentObject, params object[] options)
    {
        return selectedParameter.GetIntFromParameter(currentObject, options);
    }

    public override int ModifyIntFromParameter(int valueToModify)
    {
        return selectedParameter.ModifyIntFromParameter(valueToModify);
    }

    public override int GetMinimumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return selectedParameter.GetMinimumIntFromParameter(currentObject, options);
    }

    public override int GetMaximumIntFromParameter(BasicTiledObject currentObject = null, params object[] options)
    {
        return selectedParameter.GetMaximumIntFromParameter(currentObject, options);
    }

    public override RowSizes GenerateGridFromParameter(Vector2Int requestedGridSize_Tiles, GridLimits gridLimits)
    {
        return selectedParameter.GenerateGridFromParameter(requestedGridSize_Tiles, gridLimits);
    }

    public override int[] GenerateGridSideLengthsFromParameter(int gridLength, GridSideLimitValues sideLimitsValues)
    {
        return selectedParameter.GenerateGridSideLengthsFromParameter(gridLength, sideLimitsValues);
    }

    public override BasicTiledObject GenerateBasicTiledObjectFromParameter(params object[] options)
    {
        return selectedParameter.GenerateBasicTiledObjectFromParameter(options);
    }
}
