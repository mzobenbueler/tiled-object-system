﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_GridableObject : RuleParameter
{
    public RP_DropDownGridItemType RP_GridableObjectType;    
    public GridPositions gridPositions;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        RP_GridableObjectType = (RP_DropDownGridItemType)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownGridItemType>(this);
        gridPositions = new GridPositions();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        RP_GridableObjectType.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override void DisplayParameter()
    {
        RP_GridableObjectType.DisplayParameter();


    }

    public override BasicTiledObject GenerateBasicTiledObjectFromParameter(params object[] options)
    {
        return RP_GridableObjectType.GenerateBasicTiledObjectFromParameter(options);
    }
}
