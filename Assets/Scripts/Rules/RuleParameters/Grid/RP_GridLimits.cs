﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_GridLimits : RuleParameter
{
    [SerializeField] private RP_DropDownInt RP_GridSize_TilesX;
    [SerializeField] private RP_DropDownInt RP_GridSize_TilesY;
    [SerializeField] private RP_DropDownInt RP_GridSize_CellsX;
    [SerializeField] private RP_DropDownInt RP_GridSize_CellsY;
    [SerializeField] private RP_GridRowsSizes RP_GridRows_Sizes;

    [SerializeField] private GridLimits gridLimits;

    //UI
    bool showGridSizeTilesValues = false;
    bool showGridSizeCellsValues = false;
    bool showGridRowSizesValues = false;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        //Rule Parameters initialization
        RP_GridSize_TilesX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_TilesY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_CellsX = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridSize_CellsY = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(this);
        RP_GridRows_Sizes = (RP_GridRowsSizes)ScriptableObjectUtility.CreateAndInitialize<RP_GridRowsSizes>(this);

        gridLimits = new GridLimits();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        RP_GridSize_TilesX.RemoveNestedParametersAndSelf();
        RP_GridSize_TilesY.RemoveNestedParametersAndSelf();
        RP_GridSize_CellsX.RemoveNestedParametersAndSelf();
        RP_GridSize_CellsY.RemoveNestedParametersAndSelf();
        RP_GridRows_Sizes.RemoveNestedParametersAndSelf();
    }

    public override GridLimits GetGridLimitsFromParameter()
    {
        return gridLimits;
    }


    //UI
    public override void DisplayParameter()
    {
        showRule = EditorGUILayout.Foldout(showRule, "Grid Size");
        if (showRule)
        {
            EditorGUI.indentLevel++;

            EditorGUI.BeginChangeCheck();

            DisplayGridSizeTilesParameters();
            DisplayGridSizeCellsParameters();
            DisplayRowSizeParameters();

            EditorGUILayout.Space();

            //DisplayGridSizeCalculatedValues();

            EditorGUILayout.Space();

            EditorGUI.indentLevel--;
        }
    }

    private void DisplayGridSizeTilesParameters()
    {
        string titleSectionTiles = "Size (Tiles) : ";
        titleSectionTiles += gridLimits.gridSize_Tiles_Minimal == gridLimits.gridSize_Tiles_Maximal ?
            gridLimits.gridSize_Tiles_Minimal.ToString() :
            gridLimits.gridSize_Tiles_Minimal + " to " + gridLimits.gridSize_Tiles_Maximal;

        showGridSizeTilesValues = EditorGUILayout.Foldout(showGridSizeTilesValues, titleSectionTiles);
        if (showGridSizeTilesValues)
        {
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.LabelField("Horizontal Size");
            RP_GridSize_TilesX.DisplayParameter();
            EditorGUILayout.LabelField("Vertical Size");
            RP_GridSize_TilesY.DisplayParameter();

            if (EditorGUI.EndChangeCheck())
            {
                gridLimits.UpdateTilesLimits(
                    new Vector2Int(
                        RP_GridSize_TilesX.GetMinimumIntFromParameter(), 
                        RP_GridSize_TilesY.GetMinimumIntFromParameter()), 
                    new Vector2Int(
                        RP_GridSize_TilesX.GetMaximumIntFromParameter(), 
                        RP_GridSize_TilesY.GetMaximumIntFromParameter()));
            }
        }
    }

    private void DisplayGridSizeCellsParameters()
    {
        string titleSectionCells = "Size (Cells) : ";
        titleSectionCells += gridLimits.gridSize_Cells_Minimal == gridLimits.gridSize_Cells_Maximal ?
            gridLimits.gridSize_Cells_Minimal.ToString() :
            gridLimits.gridSize_Cells_Minimal + " to " + gridLimits.gridSize_Cells_Maximal;

        showGridSizeCellsValues = EditorGUILayout.Foldout(showGridSizeCellsValues, titleSectionCells);
        if (showGridSizeCellsValues)
        {
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.LabelField("Horizontal Size");
            RP_GridSize_CellsX.DisplayParameter();
            EditorGUILayout.LabelField("Vertical Size");
            RP_GridSize_CellsY.DisplayParameter();

            if (EditorGUI.EndChangeCheck())
            {
                gridLimits.UpdateCellsLimits(
                    new Vector2Int(
                        RP_GridSize_CellsX.GetMinimumIntFromParameter(),
                        RP_GridSize_CellsY.GetMinimumIntFromParameter()),
                    new Vector2Int(
                        RP_GridSize_CellsX.GetMaximumIntFromParameter(),
                        RP_GridSize_CellsY.GetMaximumIntFromParameter()));
            }
        }
    }

    private void DisplayRowSizeParameters()
    {
        string titleSectionRows = "Rows (Tiles) :";

        string widthsTitle = " Widths : ";
        for (int i = 0; i < gridLimits.rowSizes_Tiles_Minimal.widths.Length; i++)
        {
            widthsTitle += gridLimits.rowSizes_Tiles_Minimal.widths[i] == gridLimits.rowSizes_Tiles_Maximal.widths[i] ?
                "| " + gridLimits.rowSizes_Tiles_Minimal.widths[i].ToString() + " |" :
                "| " + gridLimits.rowSizes_Tiles_Minimal.widths[i] + " - " + gridLimits.rowSizes_Tiles_Maximal.widths[i] + " |";
        }

        string heightsTitle = " Heights : ";
        for (int i = 0; i < gridLimits.rowSizes_Tiles_Minimal.heights.Length; i++)
        {
            heightsTitle += gridLimits.rowSizes_Tiles_Minimal.heights[i] == gridLimits.rowSizes_Tiles_Maximal.heights[i] ?
                "| " + gridLimits.rowSizes_Tiles_Minimal.heights[i].ToString() + " |" :
                "| " + gridLimits.rowSizes_Tiles_Minimal.heights[i] + " - " + gridLimits.rowSizes_Tiles_Maximal.heights[i] + " |";
        }

        titleSectionRows += widthsTitle + "," + heightsTitle;

        showGridRowSizesValues = EditorGUILayout.Foldout(showGridRowSizesValues, titleSectionRows, true);
        if (showGridRowSizesValues)
        {
            EditorGUI.BeginChangeCheck();

            RP_GridRows_Sizes.DisplayParameter();

            if (EditorGUI.EndChangeCheck())
            {
                gridLimits.UpdatesRowsLimits(RP_GridRows_Sizes.GetMinimumWidths(), RP_GridRows_Sizes.GetMaximumWidths(), RP_GridRows_Sizes.GetMinimumHeights(), RP_GridRows_Sizes.GetMaximumHeights());
            }
        }
    }
}
