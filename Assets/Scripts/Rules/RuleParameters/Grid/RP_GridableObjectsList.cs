﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_GridableObjectsList : RuleParameter
{
    //[SerializeField] private List<GridTiledObjectAncien> tiledObjectsWithPosition = new List<GridTiledObjectAncien>();

    //[SerializeField] private PositionsListsAncien availableObjectsByPositions;

    [SerializeField] private GridPositionsObjects objectsListsPositions; //Objects for each positions

    [SerializeField] private List<RP_GridableObject> gridObjects; 

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        //availableObjectsByPositions = new PositionsListsAncien();

        objectsListsPositions = new GridPositionsObjects();
        gridObjects = new List<RP_GridableObject>();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        foreach(RP_GridableObject go in gridObjects)
        {
            go.RemoveNestedParametersAndSelf();
        }

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    //public override PositionsListsAncien GetPositionsListsFromParameter()
    //{
    //    return availableObjectsByPositions;
    //}

    public override GridPositionsObjects GetGridPositionsListsFromParameter()
    {
        return objectsListsPositions;
    }

    public override void DisplayParameter()
    {
        showRule = EditorGUILayout.Foldout(showRule, "Tiled Objects");
        if (showRule)
        {
            EditorGUI.BeginChangeCheck();

            DisplayAvailableTiledObjectsList();

            if (EditorGUI.EndChangeCheck())
            {
                //New
                objectsListsPositions = GridPositionsObjects.GenerateObjectPositionsLists(gridObjects);
            }
        }
    }

    private void DisplayAvailableTiledObjectsList()
    {
        /* Display the list of available tiled objects for the grid */
        if (gridObjects.Count == 0)
        {
            if (GUILayout.Button("Add Item"))
            {
                //tiledObjectsWithPosition.Add(new GridTiledObjectAncien());
                gridObjects.Add((RP_GridableObject)ScriptableObjectUtility.CreateAndInitialize<RP_GridableObject>(this));
            }
            return;
        }

        for (int i = 0; i < gridObjects.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Item"))
            {
                //tiledObjectsWithPosition.Add(new GridTiledObjectAncien());

                gridObjects.Add((RP_GridableObject)ScriptableObjectUtility.CreateAndInitialize<RP_GridableObject>(this));
            }
            if (GUILayout.Button("Delete Item"))
            {
                //tiledObjectsWithPosition.RemoveAt(i);

                gridObjects[i].RemoveNestedParametersAndSelf();
                gridObjects.RemoveAt(i);

                EditorGUILayout.EndHorizontal();
                return;
            }

            EditorGUILayout.EndHorizontal();
            
            //tiledObjectsWithPosition[i].tiledObject = (TiledObject)EditorGUILayout.ObjectField("Tiled Object", tiledObjectsWithPosition[i].tiledObject, typeof(TiledObject), false);

            //Display Object 
            gridObjects[i].DisplayParameter();

            //Display Positions : Mettre ca ailleurs
            List<bool> pos = gridObjects[i].gridPositions.activePositions;
            int positionInArrayIndex = 0;
            for (int j = 0; j < 3; j++)
            {
                EditorGUILayout.BeginHorizontal();

                for (int h = 0; h < 3; h++)
                {
                    pos[positionInArrayIndex] = EditorGUILayout.Toggle(pos[positionInArrayIndex]);
                    positionInArrayIndex++;
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}
