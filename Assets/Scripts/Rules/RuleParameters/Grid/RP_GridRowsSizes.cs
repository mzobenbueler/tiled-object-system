﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_GridRowsSizes : RuleParameter
{
    public RP_DropDownInt[] RP_GridRow_Heights;
    public RP_DropDownInt[] RP_GridRow_Widths;

    [SerializeField] private string widthsFoldoutTitle = "Widths";
    [SerializeField] private bool showWidths = false;

    [SerializeField] private string heightsFoldoutTitle = "Heights";
    [SerializeField] private bool showHeights = false;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        RP_GridRow_Heights = new RP_DropDownInt[3]
        {
            (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn),
            (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn),
            (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn)
        };

        RP_GridRow_Widths = new RP_DropDownInt[3]
        {
            (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn),
            (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn),
            (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn)
        };

        AssetDatabase.SaveAssets();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        foreach(RP_DropDownInt rp in RP_GridRow_Widths)
        {
            rp.RemoveNestedParametersAndSelf();
            AssetDatabase.RemoveObjectFromAsset(rp);
        }

        foreach (RP_DropDownInt rp in RP_GridRow_Heights)
        {
            rp.RemoveNestedParametersAndSelf();
            AssetDatabase.RemoveObjectFromAsset(rp);
        }

        AssetDatabase.RemoveObjectFromAsset(this);

        AssetDatabase.SaveAssets();
    }

    public override void DisplayParameter()
    {
        EditorGUI.indentLevel++;

        showWidths = EditorGUILayout.Foldout(showWidths, widthsFoldoutTitle, true);
        if(showWidths)
        {
            for (int i = 0; i < RP_GridRow_Widths.Length; i++)
            {
                RP_GridRow_Widths[i].DisplayParameter();
            }
        }

        showHeights = EditorGUILayout.Foldout(showHeights, heightsFoldoutTitle, true);
        if(showHeights)
        {
            for (int i = 0; i < RP_GridRow_Heights.Length; i++)
            {
                RP_GridRow_Heights[i].DisplayParameter();
            }
        }

        EditorGUI.indentLevel--;
    }

    public int[] GetMinimumHeights(BasicTiledObject currentObject = null, params object[] options)
    {
        int[] minHeights = new int[3];
        for(int i = 0; i < minHeights.Length; i ++)
        {
            minHeights[i] = RP_GridRow_Heights[i].GetMinimumIntFromParameter(currentObject, options);
        }

        return minHeights;
    }

    public int[] GetMinimumWidths(BasicTiledObject currentObject = null, params object[] options)
    {
        int[] minWidths = new int[3];
        for (int i = 0; i < minWidths.Length; i++)
        {
            minWidths[i] = RP_GridRow_Widths[i].GetMinimumIntFromParameter(currentObject, options);
        }

        return minWidths;
    }

    public int[] GetMaximumHeights(BasicTiledObject currentObject = null, params object[] options)
    {
        int[] maxHeights = new int[3];
        for (int i = 0; i < maxHeights.Length; i++)
        {
            maxHeights[i] = RP_GridRow_Heights[i].GetMaximumIntFromParameter(currentObject, options);
        }

        return maxHeights;
    }
    public int[] GetMaximumWidths(BasicTiledObject currentObject = null, params object[] options)
    {
        int[] maxWidths = new int[3];
        for (int i = 0; i < maxWidths.Length; i++)
        {
            maxWidths[i] = RP_GridRow_Widths[i].GetMaximumIntFromParameter(currentObject, options);
        }

        return maxWidths;
    }
}
