﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RP_GridGeneration_BasedOnCells : RuleParameter
{
    //public override RowSizes GenerateGridFromParameter(Vector2Int requestedGridSize_Tiles, GridLimits gridLimits)
    //{
    //    RowSizes rows = RuleTools_Grid.GenerateRowsBasedOnGridSizeCells(gridLimits);
    //    return rows;
    //}

    public override int[] GenerateGridSideLengthsFromParameter(int gridLength, GridSideLimitValues sideLimitsValues)
    {
        Debug.Log("Generating from parameter");

        int[] results = GridRuleTools.GenerationWithMaximumCells.GenerateSingleRowWithMaximumAmountOfCells(gridLength, sideLimitsValues);

        return results;
    }

    public override void DisplayParameter()
    {
        return;
    }
}
