﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RP_ModifyIntIncludeBorders : RuleParameter
{
    public override int ModifyIntFromParameter(int valueToModify)
    {
        return valueToModify;
    }
    public override void DisplayParameter()
    {

    }
}
