﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RP_ModifyIntMinusTwoBorders : RuleParameter
{
    public override int ModifyIntFromParameter(int valueToModify)
    {
        int result = valueToModify - 2;
        return result;
    }

    public override void DisplayParameter()
    {

    }
}
