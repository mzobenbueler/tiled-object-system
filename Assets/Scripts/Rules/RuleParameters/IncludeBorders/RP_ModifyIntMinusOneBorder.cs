﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RP_ModifyIntMinusOneBorder : RuleParameter
{
    public override int ModifyIntFromParameter(int valueToModify)
    {
        int result = valueToModify - 1; 
        return result;
    }
    public override void DisplayParameter()
    {

    }
}
