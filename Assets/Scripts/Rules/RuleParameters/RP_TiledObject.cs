﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_TiledObject : RuleParameter
{
    [SerializeField] private TiledObject tiledObject;

    public override void RemoveNestedParametersAndSelf()
    {
        AssetDatabase.RemoveObjectFromAsset(this);
    }

    public override BasicTiledObject GenerateBasicTiledObjectFromParameter(params object[] options)
    {
        if(!tiledObject)
        {
            Debug.LogWarning("Tiled Object Missing in " + this.mainAsset);
            return null;
        }

        BasicTiledObject obj = TiledObjectTools.GetGeneratedObject(tiledObject, options);
        return obj;
    }

    public override void DisplayParameter()
    {
        foldoutTitle = "Tiled Object (" + (tiledObject == null ? "0" : tiledObject.name ) + ")";
        showRule = EditorGUILayout.Foldout(showRule, foldoutTitle, true);
        if (!showRule)
        {
            return;
        }

        tiledObject = (TiledObject)EditorGUILayout.ObjectField(tiledObject, typeof(TiledObject), true);

        EditorGUILayout.Space();
    }
}
