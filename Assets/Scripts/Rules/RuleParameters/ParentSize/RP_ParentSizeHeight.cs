﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_ParentSizeHeight : RuleParameter
{
    public override int GetIntFromParameter(BasicTiledObject currentObject, params object[] options)
    {
        if (options.Length > 0)
        {
            Vector2Int sizeOverride = (Vector2Int)options[0];
            return sizeOverride.y;
        }
        else
        {
            return currentObject.size.y;
        }
    }
}