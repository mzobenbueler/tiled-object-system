﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RP_Tile : RuleParameter
{
    [SerializeField] private Tile tile;

    public override void RemoveNestedParametersAndSelf()
    {
        AssetDatabase.RemoveObjectFromAsset(this);
    }

    public override BasicTiledObject GenerateBasicTiledObjectFromParameter(params object[] options)
    {
        BasicTiledObject obj = new BasicTiledObject();
        obj.positionedTiles = new List<PositionedTile>();
        obj.positionedTiles.Add(new PositionedTile(tile, Vector2Int.zero));
        obj.size = Vector2Int.one;

        return obj;
    }

    public override void DisplayParameter()
    {
        foldoutTitle = "Tile (" + (tile == null ? "0" : tile.name) + ")";
        showRule = EditorGUILayout.Foldout(showRule, foldoutTitle, true);
        if (!showRule)
        {
            return;
        }

        tile = (Tile)EditorGUILayout.ObjectField(tile, typeof(Tile), true);

        EditorGUILayout.Space();
    }  
}
