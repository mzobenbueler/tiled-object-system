﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_RepeaterMode_SizeBased : RuleParameter
{
    public RP_DropDownInt offset;
    public RP_DropDownInt spaceBetweenRepeat;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        offset = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);

        spaceBetweenRepeat = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);

        AssetDatabase.SaveAssets();
    }

}
