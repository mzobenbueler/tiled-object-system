﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class RP_RepeaterSimple : RuleParameter
{
    public RP_DropDownInt offsetParameter;
    public RP_DropDownInt zoneLenghtParameter;
    public RP_DropDownInt spaceBetweenTilesParameter;
    public RP_DropDownInt quantityParameter;

    //Contained Drop downs parameters
    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        offsetParameter = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);
        zoneLenghtParameter = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);
        spaceBetweenTilesParameter = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);
        quantityParameter = (RP_DropDownInt)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownInt>(assetToSaveIn);

        AssetDatabase.SaveAssets();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        offsetParameter.RemoveNestedParametersAndSelf();
        zoneLenghtParameter.RemoveNestedParametersAndSelf();
        spaceBetweenTilesParameter.RemoveNestedParametersAndSelf();
        quantityParameter.RemoveNestedParametersAndSelf();

        //AssetDatabase.RemoveObjectFromAsset(offsetParameter);
        //AssetDatabase.RemoveObjectFromAsset(zoneLenghtParameter);
        //AssetDatabase.RemoveObjectFromAsset(spaceBetweenTilesParameter);
        //AssetDatabase.RemoveObjectFromAsset(quantityParameter);

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override void DisplayParameter()
    {
        EditorGUILayout.LabelField("Offset");
        offsetParameter.DisplayParameter();

        EditorGUILayout.LabelField("Zone Lenght");
        zoneLenghtParameter.DisplayParameter();

        EditorGUILayout.LabelField("Space between repeat");
        spaceBetweenTilesParameter.DisplayParameter();

        EditorGUILayout.LabelField("Repeat time");
        quantityParameter.DisplayParameter();

        EditorUtility.SetDirty(this);
    }

    public virtual RepeaterParameters GetRepeaterParameters()
    {
        //VOIR POUR LES PARAMETRES ICI 
        //int offset = offsetParameter.GetIntFromParameter(currentObject, options);
        //int zoneLenght = zoneLenghtParameter.GetIntFromParameter(currentObject, options);        
        //int spaceBetween = spaceBetweenTilesParameter.GetIntFromParameter(currentObject, options);
        //int quantity = quantityParameter.GetIntFromParameter(currentObject, options);

        int offset = 1;
        int zoneLenght = 1;
        int spaceBetween = 1;
        int quantity = 1;

        RepeaterParameters repeaterParameter = new RepeaterParameters(offset, zoneLenght, spaceBetween, quantity);

        return repeaterParameter; 
    }
}
