﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_ContourObject : RuleParameter
{
    public RP_DropDownGridItemType RP_GridableObjectType;
    public ContourPositions contourPosition;

    public bool showExternalPositions = false;
    public bool showInternalPositions = false;
    public bool showStraightLines = false;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        RP_GridableObjectType = (RP_DropDownGridItemType)ScriptableObjectUtility.CreateAndInitialize<RP_DropDownGridItemType>(this);
        contourPosition = new ContourPositions();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        RP_GridableObjectType.RemoveNestedParametersAndSelf();

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override void DisplayParameter()
    {
        showRule = EditorGUILayout.Foldout(showRule, "Object Param");
        if (!showRule)
        {
            return;
        }

        RP_GridableObjectType.DisplayParameter();

        showExternalPositions = EditorGUILayout.Foldout(showExternalPositions, "External Angle");
        if (showExternalPositions)
        {
            DisplayContourPositions(contourPosition.externalPositions);
        }

        showInternalPositions = EditorGUILayout.Foldout(showInternalPositions, "Internal Angle");
        if (showInternalPositions)
        {
            DisplayContourPositions(contourPosition.internalPositions);
        }

        showStraightLines = EditorGUILayout.Foldout(showStraightLines, "Straight Line");
        if (showStraightLines)
        {
            DisplayContourPositions(contourPosition.linePositions);
        }
    }

    private void DisplayContourPositions(List<bool> positions)
    {
        List<bool> pos = positions;
        int positionInArrayIndex = 0;

        for (int i = 0; i < 2; i++)
        {
            EditorGUILayout.BeginHorizontal();

            for (int j = 0; j < 2; j++)
            {
                pos[positionInArrayIndex] = EditorGUILayout.Toggle(pos[positionInArrayIndex]);
                positionInArrayIndex++;
            }

            EditorGUILayout.EndHorizontal();
        }
    }

    public override BasicTiledObject GenerateBasicTiledObjectFromParameter(params object[] options)
    {
        return RP_GridableObjectType.GenerateBasicTiledObjectFromParameter(options);
    }
}
