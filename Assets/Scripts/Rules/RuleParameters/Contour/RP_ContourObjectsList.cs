﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RP_ContourObjectsList : RuleParameter
{
    [SerializeField] private ContourPositionsObjects objectsListsPositions; //Objects for each positions

    [SerializeField] private List<RP_ContourObject> rp_ContourObjects;

    public override void InitializeParameters(ScriptableObject assetToSaveIn)
    {
        //availableObjectsByPositions = new PositionsListsAncien();

        objectsListsPositions = new ContourPositionsObjects();
        rp_ContourObjects = new List<RP_ContourObject>();
    }

    public override void RemoveNestedParametersAndSelf()
    {
        foreach (RP_ContourObject co in rp_ContourObjects)
        {
            co.RemoveNestedParametersAndSelf();
        }

        AssetDatabase.RemoveObjectFromAsset(this);
        AssetDatabase.SaveAssets();
    }

    public override ContourPositionsObjects GetContourPositionsListsFromParameter()
    {
        return objectsListsPositions;
    }

    public override void DisplayParameter()
    {
        showRule = EditorGUILayout.Foldout(showRule, "Contour Objects");
        if (showRule)
        {
            EditorGUI.BeginChangeCheck();

            DisplayAvailableTiledObjectsList();

            if (EditorGUI.EndChangeCheck())
            {
                //New
                objectsListsPositions = ContourPositionsObjects.GenerateObjectPositionsLists(rp_ContourObjects);
            }
        }
    }

    private void DisplayAvailableTiledObjectsList()
    {
        /* Display the list of available tiled objects for the grid */
        if (rp_ContourObjects.Count == 0)
        {
            if (GUILayout.Button("Add Item"))
            {
                rp_ContourObjects.Add((RP_ContourObject)ScriptableObjectUtility.CreateAndInitialize<RP_ContourObject>(this));
            }
            return;
        }

        for (int i = 0; i < rp_ContourObjects.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Item"))
            {
                rp_ContourObjects.Add((RP_ContourObject)ScriptableObjectUtility.CreateAndInitialize<RP_ContourObject>(this));
            }
            if (GUILayout.Button("Delete Item"))
            {
                rp_ContourObjects[i].RemoveNestedParametersAndSelf();
                rp_ContourObjects.RemoveAt(i);

                EditorGUILayout.EndHorizontal();
                return;
            }

            EditorGUILayout.EndHorizontal();

            //Display current Object 
            rp_ContourObjects[i].DisplayParameter();
        }
    }
}
